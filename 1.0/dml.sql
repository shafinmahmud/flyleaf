CREATE DATABASE  IF NOT EXISTS `fly_leaf` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `fly_leaf`;
-- MySQL dump 10.13  Distrib 5.7.18, for Linux (x86_64)
--
-- Host: 127.0.0.1    Database: fly_leaf
-- ------------------------------------------------------
-- Server version	5.7.18-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `activity_log`
--

LOCK TABLES `activity_log` WRITE;
/*!40000 ALTER TABLE `activity_log` DISABLE KEYS */;
INSERT INTO `activity_log` VALUES (1,'UPDATE','2016-01-01 12:00:00','test123',1);
/*!40000 ALTER TABLE `activity_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `auth_token`
--

LOCK TABLES `auth_token` WRITE;
/*!40000 ALTER TABLE `auth_token` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_token` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `author`
--

LOCK TABLES `author` WRITE;
/*!40000 ALTER TABLE `author` DISABLE KEYS */;
INSERT INTO `author` VALUES (1,'Lorem Ipsum','','Andrew Hunter'),(2,'Lorem Ipsum','','Aalen Harper'),(3,'Lorem Ipsum','','S Salvihanan'),(4,'Lorem Ipsum','','Muhammad Zafar Iqbal');
/*!40000 ALTER TABLE `author` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `author_book`
--

LOCK TABLES `author_book` WRITE;
/*!40000 ALTER TABLE `author_book` DISABLE KEYS */;
INSERT INTO `author_book` VALUES (1,1),(1,3);
/*!40000 ALTER TABLE `author_book` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `book`
--

LOCK TABLES `book` WRITE;
/*!40000 ALTER TABLE `book` DISABLE KEYS */;
INSERT INTO `book` VALUES (1,'Bangladesh','1st','','235235','Bengali',255,'Sharp Objects',1),(3,'Denmark','3rd','','548999','Bengali',300,'Sonet of Love',2),(4,'Bangladesh','4th','','3546546','Bengali',300,'Visual Basic',3),(5,'India','5th','','246876','English',456,'Computer FUndamentals Hardware and Programming',3),(6,'India','9th','','5464354','Bengali',500,'Partition and Bengal',1),(7,'Bangladesh','1st','','516546','Bengali',100,'50 Years of Politics: Through My Eyes',1),(8,'Denmark','1st','','53513','English',50,'Song of Stars',1),(9,'Germany','1st','','24564','English',523,'Relativity- The Special and The General Theory',2),(10,'Bamgladesh','1st','','45646','Bengali',60,'Mathematics Olympiad',1);
/*!40000 ALTER TABLE `book` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `book_author`
--

LOCK TABLES `book_author` WRITE;
/*!40000 ALTER TABLE `book_author` DISABLE KEYS */;
INSERT INTO `book_author` VALUES (1,1),(3,1),(5,2),(6,2),(9,4),(10,4),(7,3),(8,3);
/*!40000 ALTER TABLE `book_author` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `book_product`
--

LOCK TABLES `book_product` WRITE;
/*!40000 ALTER TABLE `book_product` DISABLE KEYS */;
/*!40000 ALTER TABLE `book_product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `book_sub_category`
--

LOCK TABLES `book_sub_category` WRITE;
/*!40000 ALTER TABLE `book_sub_category` DISABLE KEYS */;
INSERT INTO `book_sub_category` VALUES (1,1),(3,3),(4,5),(5,7),(6,9),(7,11),(8,12),(9,13),(10,14);
/*!40000 ALTER TABLE `book_sub_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `cart`
--

LOCK TABLES `cart` WRITE;
/*!40000 ALTER TABLE `cart` DISABLE KEYS */;
/*!40000 ALTER TABLE `cart` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `category`
--

LOCK TABLES `category` WRITE;
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
INSERT INTO `category` VALUES (2,'Computer, Internet & Freelancing'),(3,'History In Tradition'),(4,'Mathematics, Science & Technology'),(1,'Translation');
/*!40000 ALTER TABLE `category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `order`
--

LOCK TABLES `order` WRITE;
/*!40000 ALTER TABLE `order` DISABLE KEYS */;
/*!40000 ALTER TABLE `order` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `ordered_product`
--

LOCK TABLES `ordered_product` WRITE;
/*!40000 ALTER TABLE `ordered_product` DISABLE KEYS */;
/*!40000 ALTER TABLE `ordered_product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `product`
--

LOCK TABLES `product` WRITE;
/*!40000 ALTER TABLE `product` DISABLE KEYS */;
INSERT INTO `product` VALUES (1,4.5,120,0,120,1,5,1,1,1),(2,3.5,150,0,150,1,10,3,1,1),(4,4,200,0,200,1,1,4,3,0),(5,2.5,210,0,210,1,2,5,3,0),(6,2.5,150,0,150,1,3,6,4,0),(7,5,140,0,140,1,4,7,4,0),(8,2,300,0,300,1,1,8,5,0),(9,1.5,123,0,123,1,20,9,5,0),(10,0,230,0,230,1,3,10,5,0);
/*!40000 ALTER TABLE `product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `publisher`
--

LOCK TABLES `publisher` WRITE;
/*!40000 ALTER TABLE `publisher` DISABLE KEYS */;
INSERT INTO `publisher` VALUES (1,NULL,'Creative Media Publisher','Lorem Ipsum'),(2,'','T-Fountain','Lorem Ipsum'),(3,'','O\'Reilly Media Ink','Engineering');
/*!40000 ALTER TABLE `publisher` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `role`
--

LOCK TABLES `role` WRITE;
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT INTO `role` VALUES (1,'Admin'),(2,'User');
/*!40000 ALTER TABLE `role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `sale_log`
--

LOCK TABLES `sale_log` WRITE;
/*!40000 ALTER TABLE `sale_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `sale_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `store`
--

LOCK TABLES `store` WRITE;
/*!40000 ALTER TABLE `store` DISABLE KEYS */;
INSERT INTO `store` VALUES (1,'','Store1','APPROVED',2),(3,'','Store2','APPROVED',2),(4,'','Normal User Store1','APPROVED',4),(5,'','Normal User Store2','APPROVED',4);
/*!40000 ALTER TABLE `store` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `subcategory`
--

LOCK TABLES `subcategory` WRITE;
/*!40000 ALTER TABLE `subcategory` DISABLE KEYS */;
INSERT INTO `subcategory` VALUES (1,'Novel',1),(3,'Poem',1),(5,'Software',2),(7,'Hardware & Troubleshooting',2),(9,'Indian',3),(11,'Liberation War',3),(12,'Cosmology',4),(13,'Physics',4),(14,'Mathematics',4);
/*!40000 ALTER TABLE `subcategory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'Dhaka','I m admin','1993-07-03 00:00:00','admin@therapservices.net','Admin','MALE','','','','admin','01945','2016-12-26 07:04:33'),(2,'Sylhet','I m shopkeeper','1993-09-01 00:00:00','user@therapservices.net','Joe','MALE','','','Smith','user','091374','2016-01-01 06:00:00'),(3,'Dhaka','I m not approved yet','1992-09-07 00:00:00','unapproved@therapservices.net','Lila','FEMALE','','\0','Boti','user','45412','2016-01-01 06:00:00'),(4,'Dhaka','Normal User and Shopkeeper','2015-12-25 10:36:06','normal@therapservices.net','Mir','MALE','','','Kashim','user','23425','2016-01-01 06:00:00');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `user_product_rating`
--

LOCK TABLES `user_product_rating` WRITE;
/*!40000 ALTER TABLE `user_product_rating` DISABLE KEYS */;
INSERT INTO `user_product_rating` VALUES (1,5,1,4),(2,3.5,2,4),(3,4.5,4,4),(4,2.5,5,4),(5,4,6,4),(6,1.5,7,4),(7,5,8,4),(8,1.5,9,4),(9,0,10,4);
/*!40000 ALTER TABLE `user_product_rating` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `user_role`
--

LOCK TABLES `user_role` WRITE;
/*!40000 ALTER TABLE `user_role` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `verification_token`
--

LOCK TABLES `verification_token` WRITE;
/*!40000 ALTER TABLE `verification_token` DISABLE KEYS */;
/*!40000 ALTER TABLE `verification_token` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-05-14 15:31:44