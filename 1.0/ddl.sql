CREATE TABLE activity_log (
  id            BIGINT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  activity_type VARCHAR(255)       NOT NULL,
  created_at    DATETIME           NOT NULL,
  message       VARCHAR(255)       NOT NULL,
  user_id       BIGINT
);

CREATE TABLE auth_token (
  id         BIGINT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  expires_on DATETIME,
  token      VARCHAR(255),
  user_id    BIGINT
);

CREATE TABLE author (
  id        BIGINT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  details   VARCHAR(255),
  image_url VARCHAR(255)       NOT NULL,
  name      VARCHAR(255)
);

CREATE TABLE author_book (
  author_id BIGINT NOT NULL,
  book_id   BIGINT NOT NULL
);

CREATE TABLE book (
  id           BIGINT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  country      VARCHAR(255),
  edition      VARCHAR(255),
  image_url    VARCHAR(255)       NOT NULL,
  isbn         VARCHAR(255),
  language     VARCHAR(255)       NOT NULL,
  no_of_pages  INT,
  title        VARCHAR(255)       NOT NULL,
  publisher_id BIGINT
);

CREATE TABLE book_author (
  book_id   BIGINT NOT NULL,
  author_id BIGINT NOT NULL
);

CREATE TABLE book_product (
  book_id        BIGINT NOT NULL,
  productList_id BIGINT NOT NULL
);

CREATE TABLE cart (
  id              BIGINT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  quantity        INT                NOT NULL,
  sub_total_price DOUBLE,
  product_id      BIGINT,
  user_id         BIGINT
);

CREATE TABLE category (
  id   BIGINT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  name VARCHAR(255)       NOT NULL
);

CREATE TABLE order (
  id                   BIGINT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  bkash_transaction_id VARCHAR(255),
  checkout_time        DATETIME,
  delivery_date        DATETIME,
  destination          VARCHAR(255),
  order_status         VARCHAR(255),
  order_type           VARCHAR(255),
  payment_option       VARCHAR(255),
  shipping_cost        DOUBLE,
  total_price          DOUBLE,
  user_id              BIGINT
);

CREATE TABLE ordered_product (
  id              BIGINT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  quantity        INT                NOT NULL,
  sub_total_price DOUBLE,
  order_id        BIGINT,
  product_id      BIGINT
);

CREATE TABLE product (
  id             BIGINT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  avg_rating     DOUBLE,
  base_price     DOUBLE,
  discount       DOUBLE             NOT NULL,
  purchase_price DOUBLE,
  rating_count   INT,
  stock_quantity INT,
  book_id        BIGINT,
  store_id       BIGINT
);

CREATE TABLE publisher (
  id             BIGINT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  image_url      VARCHAR(255),
  name           VARCHAR(255)       NOT NULL,
  publisher_info VARCHAR(255)
);

CREATE TABLE role (
  id   BIGINT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  name VARCHAR(255)       NOT NULL
);

CREATE TABLE sale_log (
  id         BIGINT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  sold_at    DATETIME           NOT NULL,
  quantity   BIGINT             NOT NULL,
  sale_price DOUBLE             NOT NULL,
  product_id BIGINT,
  user_id    BIGINT
);

CREATE TABLE store (
  id        BIGINT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  image_url VARCHAR(255),
  name      VARCHAR(255),
  status    VARCHAR(255),
  user_id   BIGINT
);

CREATE TABLE subcategory (
  id          BIGINT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  name        VARCHAR(255)       NOT NULL,
  category_id BIGINT
);

CREATE TABLE user (
  id          BIGINT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  address     VARCHAR(255),
  bio         VARCHAR(255),
  dob         DATETIME,
  email       VARCHAR(255)       NOT NULL,
  first_name  VARCHAR(255)       NOT NULL,
  gender      VARCHAR(255),
  image_url   VARCHAR(255),
  is_approved BIT                NOT NULL,
  last_name   VARCHAR(255)       NOT NULL,
  password    VARCHAR(255)       NOT NULL,
  phone       VARCHAR(255)
);
CREATE TABLE user_product_rating
(
  id           BIGINT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  rating_value REAL,
  product_id   BIGINT,
  user_id      BIGINT
);
CREATE TABLE user_role
(
  user_id BIGINT NOT NULL,
  role_id BIGINT NOT NULL
);
CREATE TABLE verification_token
(
  id         BIGINT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  expires_on DATETIME,
  is_used    BIT,
  token      VARCHAR(255),
  user_id    BIGINT
);
ALTER TABLE activity_log
  ADD FOREIGN KEY (user_id) REFERENCES user (id);
CREATE INDEX FK_csf0njobhrvcaaryhn9ya8vje
  ON activity_log (user_id);
ALTER TABLE auth_token
  ADD FOREIGN KEY (user_id) REFERENCES user (id);
CREATE INDEX FK_aiqc20kpjasth5bxogsragoif
  ON auth_token (user_id);
ALTER TABLE author_book
  ADD FOREIGN KEY (author_id) REFERENCES author (id);
ALTER TABLE author_book
  ADD FOREIGN KEY (book_id) REFERENCES author (id);
CREATE INDEX FK_3756au12qe5s0ee22r4tbfj54
  ON author_book (author_id);
CREATE INDEX FK_bfff0qy5yokmeel0q0e10msv7
  ON author_book (book_id);
ALTER TABLE book
  ADD FOREIGN KEY (publisher_id) REFERENCES publisher (id);
CREATE INDEX FK_19ss4s5ji828yqdpgm0otr93s
  ON book (publisher_id);
ALTER TABLE book_author
  ADD FOREIGN KEY (author_id) REFERENCES author (id);
ALTER TABLE book_author
  ADD FOREIGN KEY (book_id) REFERENCES book (id);
CREATE INDEX FK_6cmg2roopa9a4c97uxetgf2e9
  ON book_author (author_id);
CREATE INDEX FK_q37qkj7serxg0bh56m450uigs
  ON book_author (book_id);
ALTER TABLE book_product
  ADD FOREIGN KEY (book_id) REFERENCES book (id);
ALTER TABLE book_product
  ADD FOREIGN KEY (productList_id) REFERENCES product (id);
CREATE UNIQUE INDEX UK_33mbyx166gqxe9greh2hk95tn
  ON book_product (productList_id);
CREATE INDEX FK_1uikgbta4n8ca519nfw0ym56r
  ON book_product (book_id);
ALTER TABLE cart
  ADD FOREIGN KEY (user_id) REFERENCES user (id);
ALTER TABLE cart
  ADD FOREIGN KEY (product_id) REFERENCES product (id);
CREATE INDEX FK_9emlp6m95v5er2bcqkjsw48he
  ON cart (user_id);
CREATE INDEX FK_ey2t23ju6wbpsypqcd6rnm0go
  ON cart (product_id);
CREATE UNIQUE INDEX UK_46ccwnsi9409t36lurvtyljak
  ON category (name);
ALTER TABLE ` order ` ADD FOREIGN KEY (user_id) REFERENCES user ( id );
CREATE INDEX FK_mh40cn97o5svvy5c32ws9tnvp
  ON ` order ` (user_id);
ALTER TABLE ordered_product
  ADD FOREIGN KEY (product_id) REFERENCES product (id);
ALTER TABLE ordered_product
  ADD FOREIGN KEY (order_id) REFERENCES ` order ` ( id );
CREATE INDEX FK_2403i5fqpo2rg62srhavu18t3
  ON ordered_product (product_id);
CREATE INDEX FK_nr42e4pbkd60bjey5ta1rssey
  ON ordered_product (order_id);
ALTER TABLE product
  ADD FOREIGN KEY (book_id) REFERENCES book (id);
ALTER TABLE product
  ADD FOREIGN KEY (store_id) REFERENCES store (id);
CREATE INDEX FK_6j1r1vq1htsy8wd7lrwvgl1nj
  ON product (book_id);
CREATE INDEX FK_j9qchw9ki2is6psdc7uuujyqx
  ON product (store_id);
CREATE UNIQUE INDEX UK_h9trv4xhmh6s68vbw9ba6to70
  ON publisher (name);
CREATE UNIQUE INDEX UK_8sewwnpamngi6b1dwaa88askk
  ON role (name);
ALTER TABLE sale_log
  ADD FOREIGN KEY (product_id) REFERENCES product (id);
ALTER TABLE sale_log
  ADD FOREIGN KEY (user_id) REFERENCES user (id);
CREATE INDEX FK_1u7hwpm0di8masdqfcvxcci1v
  ON sale_log (product_id);
CREATE INDEX FK_d0arsejaacfvpdipf61cfgk0t
  ON sale_log (user_id);
ALTER TABLE store
  ADD FOREIGN KEY (user_id) REFERENCES user (id);
CREATE UNIQUE INDEX UK_d0p5ly1cv6guij7sq1mbnr8ec
  ON store (name);
CREATE INDEX FK_bjqth9sjo3phwbmybuysm65be
  ON store (user_id);
ALTER TABLE subcategory
  ADD FOREIGN KEY (category_id) REFERENCES category (id);
CREATE UNIQUE INDEX UK_e060alu3238gwu0mvhgh6xkhd
  ON subcategory (name);
CREATE INDEX FK_dglte9qeu8l5fhggto4loyegg
  ON subcategory (category_id);
CREATE UNIQUE INDEX UK_ob8kqyqqgmefl0aco34akdtpe
  ON user (email);
ALTER TABLE user_product_rating
  ADD FOREIGN KEY (product_id) REFERENCES product (id);
ALTER TABLE user_product_rating
  ADD FOREIGN KEY (user_id) REFERENCES user (id);
CREATE INDEX FK_5e2dt2ncmomhxxe8i1xf2xqw4
  ON user_product_rating (product_id);
CREATE INDEX FK_ikl6ycp805md4ht57yp32opog
  ON user_product_rating (user_id);
ALTER TABLE user_role
  ADD FOREIGN KEY (user_id) REFERENCES user (id);
ALTER TABLE user_role
  ADD FOREIGN KEY (role_id) REFERENCES role (id);
CREATE INDEX FK_apcc8lxk2xnug8377fatvbn04
  ON user_role (user_id);
CREATE INDEX FK_it77eq964jhfqtu54081ebtio
  ON user_role (role_id);
ALTER TABLE verification_token
  ADD FOREIGN KEY (user_id) REFERENCES user (id);
CREATE INDEX FK_q6jibbenp7o9v6tq178xg88hg
  ON verification_token (user_id);