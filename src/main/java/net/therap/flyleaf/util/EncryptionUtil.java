package net.therap.flyleaf.util;

import org.mindrot.jbcrypt.BCrypt;

/**
 * @author shafin
 * @since 12/26/2016
 */
public class EncryptionUtil {

    public static String generateSecureHash(String originalString) {
        return BCrypt.hashpw(originalString, BCrypt.gensalt(12));
    }

    public static boolean matchWithSecureHash(String providedString, String existingHash) {
        return BCrypt.checkpw(providedString, existingHash);
    }
}
