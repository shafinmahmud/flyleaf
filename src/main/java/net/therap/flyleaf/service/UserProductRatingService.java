package net.therap.flyleaf.service;

import net.therap.flyleaf.dao.UserProductRatingDao;
import net.therap.flyleaf.domain.UserProductRating;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author nourin
 * @author shafin
 * @since 12/29/16
 */
@Service
public class UserProductRatingService {

    @Autowired
    private UserProductRatingDao userProductRatingDao;

    public void save(UserProductRating userProductRating) {
        userProductRatingDao.save(userProductRating);
    }

    public long getUserProductRatingCount(long userId) {
        return userProductRatingDao.countRows("user_id", userId);
    }
}
