package net.therap.flyleaf.service;

import net.therap.flyleaf.dao.UserDao;
import net.therap.flyleaf.domain.User;
import net.therap.flyleaf.util.EncryptionUtil;
import net.therap.flyleaf.web.command.SignUpForm;
import net.therap.flyleaf.web.command.UserProfileForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Date;

/**
 * @author shafin
 * @since 12/25/16
 */
@Transactional
@Service(value = "userService")
public class UserService {

    @Autowired
    private UserDao userDao;

    @Autowired
    private RoleService roleService;

    public User getUser(long id) {
        User user = userDao.findBy("id", id);
        user.getRoleList().size();

        return user;
    }

    public User getUser(String userEmail) {
        User user = userDao.findBy("email", userEmail);
        user.getRoleList().size();

        return user;
    }

    public void signUpUser(SignUpForm signUpForm) {

        User user = new User();
        user.setFirstName(signUpForm.getFirstName());
        user.setLastName(signUpForm.getLastName());
        user.setEmail(signUpForm.getEmail());
        user.setAddress(signUpForm.getAddress());
        user.setDateOfBirth(signUpForm.getDateOfBirth());
        user.setPhone(signUpForm.getPhone());
        user.setGender(signUpForm.getGender());
        user.setPassword(EncryptionUtil.generateSecureHash(signUpForm.getPassword()));
        user.setApproved(false);
        user.setCreatedAt(new Date());

        user.setRoleList(roleService.getUserRoleList());

        userDao.save(user);
    }

    public void updateUser(long id, UserProfileForm userProfile) {

        User user = userDao.find(id);
        user.setFirstName(userProfile.getFirstName());
        user.setLastName(userProfile.getLastName());
        user.setGender(userProfile.getGender());
        user.setDateOfBirth(userProfile.getDateOfBirth());
        user.setPhone(userProfile.getPhone());
        user.setAddress(userProfile.getAddress());
        user.setBio(userProfile.getBio());

        userDao.save(user);
    }

    public void insertUser(User user) {
        userDao.save(user);
    }
}
