package net.therap.flyleaf.service;

import net.therap.flyleaf.dao.RoleDao;
import net.therap.flyleaf.domain.Role;
import net.therap.flyleaf.enumerator.RoleType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

/**
 * @author shafin
 * @since 12/26/2016
 */
@Transactional
@Service(value = "roleService")
public class RoleService {

    @Autowired
    private RoleDao roleDao;

    public List<Role> getUserRoleList() {
        List<Role> roleList = new ArrayList<>();

        Role role = roleDao.findBy("name", RoleType.USER);
        if (role != null) {
            roleList.add(role);
            return roleList;
        }

        role = new Role(RoleType.USER);
        roleDao.save(role);
        roleList.add(role);

        return roleList;
    }

    public List<Role> getAdminRoleList() {
        List<Role> roleList = new ArrayList<>();

        Role adminRole = roleDao.findBy("name", RoleType.ADMIN);
        if (adminRole == null) {
            adminRole.setName(RoleType.ADMIN);
            roleDao.save(adminRole);
        }

        Role userRole = roleDao.findBy("name", RoleType.USER);
        if (userRole == null) {
            userRole.setName(RoleType.USER);
            roleDao.save(userRole);
        }

        roleList.add(adminRole);
        roleList.add(userRole);

        return roleList;
    }
}
