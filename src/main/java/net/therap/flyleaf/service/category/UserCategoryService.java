package net.therap.flyleaf.service.category;

import net.therap.flyleaf.dao.CategoryDao;
import net.therap.flyleaf.domain.Category;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

/**
 * @author arafat
 * @since 12/22/16
 */
@Service
@Transactional
public class UserCategoryService {

    @Autowired
    private CategoryDao categoryDao;

    public List<Category> getPopularCategories() {
        return categoryDao.getPopularCategories();
    }
}