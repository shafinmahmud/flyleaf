package net.therap.flyleaf.service;

import net.therap.flyleaf.domain.Order;
import net.therap.flyleaf.domain.OrderedProduct;
import net.therap.flyleaf.domain.Product;
import net.therap.flyleaf.domain.User;
import net.therap.flyleaf.util.DateUtil;
import net.therap.flyleaf.web.command.OrderInfo;
import net.therap.flyleaf.web.command.ProductInfo;

import java.util.ArrayList;
import java.util.List;

/**
 * @author shafin
 * @since 12/29/16
 */
public class UserServiceHelper {

    public static OrderInfo getOrderInfoFromEntity(Order order) {

        OrderInfo orderInfo = new OrderInfo();
        orderInfo.setOrderId(order.getId());
        orderInfo.setCheckoutDate(DateUtil.formatDate(order.getCheckoutTime(), "dd, MMM, YYYY"));
        orderInfo.setDeliveryDate(DateUtil.formatDate(order.getDeliveryDate(), "dd, MMM, YYYY"));
        orderInfo.setPaymentInfo(order.getPaymentOption().toString());
        orderInfo.setOrderStatus(order.getOrderStatus().toString());

        User user = order.getUser();
        StringBuilder userAddress = new StringBuilder();
        userAddress.append(user.getFirstName()).append(" ")
                .append(user.getLastName()).append("\n")
                .append(user.getAddress()).append("\n")
                .append(user.getPhone());

        orderInfo.setBilledTo(userAddress.toString());
        orderInfo.setShippingAddress(order.getDestination());

        orderInfo.setSubTotalPrice(order.getTotalPrice() + order.getShippingCost());
        orderInfo.setTotalPrice(order.getTotalPrice());
        orderInfo.setShippingCost(order.getShippingCost());

        orderInfo.setOrderStatus(order.getOrderStatus().toString());

        List<ProductInfo> productInfoList = new ArrayList<>();
        for (OrderedProduct orderedProduct : order.getOrderedProductList()) {
            productInfoList.add(getProductInfoFromEntity(orderedProduct));
        }

        orderInfo.setProductList(productInfoList);

        return orderInfo;
    }

    public static ProductInfo getProductInfoFromEntity(OrderedProduct orderedProduct) {
        ProductInfo productInfo = new ProductInfo();
        productInfo.setQuantity(orderedProduct.getQuantity());
        productInfo.setPrice(orderedProduct.getSubTotalPrice() / orderedProduct.getQuantity());
        productInfo.setSubTotal(orderedProduct.getSubTotalPrice());

        Product product = orderedProduct.getProduct();
        mapProductInfoFromEntity(productInfo, product);

        return productInfo;
    }

    public static ProductInfo mapProductInfoFromEntity(ProductInfo productInfo, Product product) {
        productInfo.setProductId(product.getId());
        productInfo.setAuthor(product.getBook().getAuthorString());
        productInfo.setAvgRating(product.getAvgRating());
        productInfo.setBookTitle(product.getBook().getTitle());
        productInfo.setProductPhoto(product.getBook().getImageUrl());

        return productInfo;
    }
}
