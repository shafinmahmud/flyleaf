package net.therap.flyleaf.service.storekeeper;

import net.therap.flyleaf.dao.AuthorDao;
import net.therap.flyleaf.domain.Author;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

/**
 * @author al-amin
 * @since 12/27/16
 */
@Service
@Transactional
public class MyAuthorService {

    @Autowired
    private AuthorDao authorDao;

    public List<Author> getAuthors() {
        return authorDao.findAll();
    }

    public Author getAuthor(long id) {
        return authorDao.findBy("id", id);
    }

    public Author addAuthor(Author author) {
        return authorDao.addAuthor(author);
    }

    public List<Author> getSelectedAuthors(List<Long> selectedAuthorIndexes) {
        List<Author> authorList = new ArrayList<>();
        for (Long selectedAuthorIndex : selectedAuthorIndexes) {
            if (selectedAuthorIndex == 0L) {
                Author author = new Author();
                author.setImageUrl("1");
                authorList.add(author);
            } else {
                authorList.add(authorDao.findBy("id", selectedAuthorIndex));
            }
        }
        
        return authorList;
    }
}
