package net.therap.flyleaf.service.storekeeper;

import net.therap.flyleaf.dao.BookDao;
import net.therap.flyleaf.domain.Book;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

/**
 * @author al-amin
 * @since 12/26/16
 */
@Service
@Transactional
public class MyBookService {

    @Autowired
    private BookDao bookDao;

    public Book addBook(Book book) {
        return bookDao.addBook(book);
    }

    public List<Book> getBooks() {
        return bookDao.findAll();
    }

    public Book getBook(long id) {
        return bookDao.findBy("id", id);
    }
}