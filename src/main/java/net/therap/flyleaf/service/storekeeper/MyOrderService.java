package net.therap.flyleaf.service.storekeeper;

import net.therap.flyleaf.dao.MyOrderDao;
import net.therap.flyleaf.dao.UserDao;
import net.therap.flyleaf.domain.OrderedProduct;
import net.therap.flyleaf.domain.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author Al-Amin
 * @since 12/31/2016
 */
@Service
@Transactional
public class MyOrderService {

    @Autowired
    private MyOrderDao myOrderDao;

    @Autowired
    private UserDao userDao;

    public List<OrderedProduct> getMyOrders(User user, int page, int size) {
        return myOrderDao.getMyOrders(userDao.find(user.getId()), page, size);
    }

    public long countMyNextOrders(User user, int page, int size) {
        return myOrderDao.getMyOrders(userDao.find(user.getId()), page, size).size();
    }
}
