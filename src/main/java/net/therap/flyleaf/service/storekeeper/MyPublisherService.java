package net.therap.flyleaf.service.storekeeper;

import net.therap.flyleaf.dao.PublisherDao;
import net.therap.flyleaf.domain.Publisher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

/**
 * @author al-amin
 * @since 12/27/16
 */
@Service
@Transactional
public class MyPublisherService {

    @Autowired
    private PublisherDao publisherDao;

    public List<Publisher> getPublishers() {
        return publisherDao.findAll();
    }

    public Publisher getPublisher(long id) {
        return publisherDao.findBy("id", id);
    }

    public Publisher addPublisher(Publisher publisher) {
        return publisherDao.addPublisher(publisher);
    }
}
