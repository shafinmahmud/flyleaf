package net.therap.flyleaf.service.storekeeper;

import net.therap.flyleaf.dao.UserDao;
import net.therap.flyleaf.domain.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

/**
 * @author al-amin
 * @since 12/27/16
 */
@Service
@Transactional
public class MyUserService {

    @Autowired
    private UserDao userDao;

    public User getUser(long id) {
        return userDao.findBy("id", id);
    }
}
