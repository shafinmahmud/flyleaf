package net.therap.flyleaf.service.admin;

import net.therap.flyleaf.dao.StoreDao;
import net.therap.flyleaf.domain.Store;
import net.therap.flyleaf.enumerator.Status;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

/**
 * @author subrata
 * @author nourin
 * @since 12/26/16
 */
@Service(value = "adminStoreService")
@Transactional
public class AdminStoreService {

    @Autowired
    private StoreDao storeDao;

    public List<Store> getStoreList() {
        return storeDao.findAllStoreOrderByStatus();
    }

    public void verifyStore(Store store) {
        Status status = store.getStatus();
        store = storeDao.find(store.getId());
        store.setStatus(status);
        storeDao.update(store);
    }

    public Store getStore(Store store) {
        return storeDao.find(store.getId());
    }

    public List<Store> getStores(int pageNumber, int pageSize) {
        return storeDao.findAll(pageNumber, pageSize);
    }

    public Long findCount() {
        return storeDao.findCount();
    }

    public long countPendingStores() {
        return storeDao.findAllBy("status", Status.PENDING).size();
    }

    public Store getSearchResult(String searchKeyWord) {
        return storeDao.doesExist(searchKeyWord);
    }
}