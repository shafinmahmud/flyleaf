package net.therap.flyleaf.service.admin;

import net.therap.flyleaf.dao.CategoryDao;
import net.therap.flyleaf.dao.SubCategoryDao;
import net.therap.flyleaf.domain.Category;
import net.therap.flyleaf.domain.SubCategory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

/**
 * @author subrata
 * @author nourin
 * @since 12/22/16
 */
@Service
@Transactional
public class AdminCategoryService {

    @Autowired
    CategoryDao categoryDao;

    @Autowired
    SubCategoryDao subCategoryDao;

    public List<Category> getCategories() {
        return categoryDao.findAll();
    }

    public List<Category> getCategories(int pageNumber, int pageSize) {
        return categoryDao.findAll(pageNumber, pageSize);
    }

    public Long findCount() {
        return categoryDao.findCount();
    }

    public void addCategory(Category category) {
        categoryDao.save(category);
    }

    public void updateCategory(Category category) {
        if (categoryDao.findBy("name", category.getName()) == null) {
            String newName = category.getName();
            category = categoryDao.find(category.getId());
            category.setName(newName);
            categoryDao.update(category);
        }
    }

    public Category getCategory(long id) {
        Category category = categoryDao.find(id);
        category.getSubCategoryList().size();

        return category;
    }

    public void addSubCategory(int categoryId, SubCategory subCategory) {
        if (subCategoryDao.findBy("name", subCategory.getName()) == null) {
            subCategory.setCategory(categoryDao.find(categoryId));
            subCategoryDao.save(subCategory);
        }
    }

    public void updateSubCategory(SubCategory subCategory) {
        if (subCategoryDao.findBy("name", subCategory.getName()) == null) {
            String newName = subCategory.getName();
            subCategory = subCategoryDao.find(subCategory.getId());
            subCategory.setName(newName);
            subCategoryDao.update(subCategory);
        }
    }
    
    public Category getSearchResult(String searchKeyWord) {
        return categoryDao.doesExist(searchKeyWord);
    }
}