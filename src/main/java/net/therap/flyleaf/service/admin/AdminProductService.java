package net.therap.flyleaf.service.admin;

import net.therap.flyleaf.dao.ProductDao;
import net.therap.flyleaf.dao.StoreDao;
import net.therap.flyleaf.domain.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

/**
 * @author subrata
 * @since 12/22/16
 */
@Service
@Transactional
public class AdminProductService {

    @Autowired
    private ProductDao productDao;

    @Autowired
    private StoreDao storeDao;

    public List<Product> getNewProducts() {
        List<Product> newProductList = productDao.findAllBy("is_verified", 0);
        for (Product product : newProductList) {
            product.getBook().getAuthorList().size();
        }

        return newProductList;
    }

    public void verifyNewProduct(Product product) {
        int isVerified = product.getIsVerified();
        product = productDao.find(product.getId());
        product.setIsVerified(isVerified);
        productDao.update(product);
    }

    public List<Product> getProducts(long storeId) {
        List<Product> productList = productDao.findAllBy("store", storeDao.find(storeId));
        for (Product product : productList) {
            product.getBook().getAuthorList().size();
        }

        return productList;
    }

    public long countNotVerifiedProducts() {
        return productDao.findAllBy("isVerified", 0).size();
    }
}