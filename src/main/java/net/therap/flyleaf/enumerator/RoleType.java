package net.therap.flyleaf.enumerator;

/**
 * @author shafin
 * @since 12/26/2016
 */
public enum RoleType {
    ADMIN, USER
}
