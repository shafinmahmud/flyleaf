package net.therap.flyleaf.enumerator;

/**
 * @author shafin
 * @since 12/22/16
 */
public enum ActivityType {
    UPDATE, INSERT, DELETE, PURCHASE, SIGNUP, EMAIL_VERIFICATION, PASSWORD_CHANGE,
    PROFILE_UPDATE, PASSWORD_RECOVERY, LOGIN, LOGOUT, REVIEW
}
