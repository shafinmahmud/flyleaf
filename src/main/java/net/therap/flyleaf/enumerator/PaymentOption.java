package net.therap.flyleaf.enumerator;

/**
 * @author shafin
 * @since 12/22/16
 */
public enum PaymentOption {
    CASH_ON_DELIVERY, BKASH
}
