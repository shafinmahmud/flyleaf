package net.therap.flyleaf.enumerator;

/**
 * @author shafin
 * @since 12/22/16
 */
public enum Gender {
    MALE, FEMALE
}
