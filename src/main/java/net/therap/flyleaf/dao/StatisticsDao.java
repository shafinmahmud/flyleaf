package net.therap.flyleaf.dao;

import net.therap.flyleaf.domain.SaleLog;
import org.springframework.stereotype.Repository;

/**
 * @author Al-Amin
 * @since 12/26/2016
 */
@Repository
public class StatisticsDao extends AbstractDao<SaleLog> {
}
