package net.therap.flyleaf.dao;

import net.therap.flyleaf.domain.Book;
import org.springframework.stereotype.Repository;

/**
 * @author al-amin
 * @since 12/22/16
 */
@Repository
public class BookDao extends AbstractDao<Book> {
    public Book addBook(Book book) {
        em.persist(book);
        em.flush();

        return book;
    }
}
