package net.therap.flyleaf.dao;

import net.therap.flyleaf.domain.Order;
import net.therap.flyleaf.domain.User;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author mezan
 * @author shafin
 * @since 12/22/16.
 */
@Repository
public class OrderDao extends AbstractDao<Order> {

    @SuppressWarnings("unchecked")
    public Order findMostRecentOrder(User user) {
        List<Order> list = (List<Order>) em.createQuery("FROM Order o WHERE user = :user  order by o.checkoutTime desc")
                .setParameter("user", user)
                .getResultList();

        return !list.isEmpty() ? list.get(0) : null;
    }

}
