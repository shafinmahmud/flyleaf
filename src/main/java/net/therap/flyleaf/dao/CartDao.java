package net.therap.flyleaf.dao;

import net.therap.flyleaf.domain.Cart;
import org.springframework.stereotype.Repository;

/**
 * @author mezan
 * @since 12/22/16.
 */
@Repository
public class CartDao extends AbstractDao<Cart> {
}
