package net.therap.flyleaf.dao;

import net.therap.flyleaf.domain.OrderedProduct;
import net.therap.flyleaf.domain.User;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Al-Amin
 * @since 12/31/2016
 */
@Repository
public class MyOrderDao extends AbstractDao<OrderedProduct> {

    public List<OrderedProduct> getMyOrders(User user, int page, int size) {
        String sql = "SELECT * FROM ordered_product WHERE product_id IN(SELECT id FROM product WHERE store_id IN(SELECT id" +
                " FROM store WHERE user_id = " + user.getId() + "))";

        return em.createNativeQuery(sql, OrderedProduct.class)
                .setFirstResult(page * size)
                .setMaxResults(size)
                .getResultList();
    }
}
