package net.therap.flyleaf.dao;

import net.therap.flyleaf.domain.SubCategory;
import org.springframework.stereotype.Repository;

/**
 * @author subrata
 * @since 12/25/16
 */
@Repository
public class SubCategoryDao extends AbstractDao<SubCategory> {
}
