package net.therap.flyleaf.dao;

import net.therap.flyleaf.domain.UserProductRating;
import org.springframework.stereotype.Repository;

/**
 * @author shafin
 * @since 12/29/16
 */
@Repository
public class UserProductRatingDao extends AbstractDao<UserProductRating> {
}
