package net.therap.flyleaf.dao;

import net.therap.flyleaf.domain.SaleLog;
import org.springframework.stereotype.Repository;

/**
 * @author subrata
 * @since 12/22/16
 */
@Repository
public class SaleLogDao extends AbstractDao<SaleLog> {
}
