package net.therap.flyleaf.dao;

import net.therap.flyleaf.domain.VerificationToken;
import org.springframework.stereotype.Repository;

/**
 * @author shafin
 * @since 12/26/16
 */
@Repository
public class VerificationTokenDao extends AbstractDao<VerificationToken> {
}
