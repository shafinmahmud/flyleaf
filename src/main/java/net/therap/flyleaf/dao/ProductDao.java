package net.therap.flyleaf.dao;

import net.therap.flyleaf.domain.Product;
import net.therap.flyleaf.domain.Store;
import net.therap.flyleaf.web.command.ChoiceListCommand;
import org.springframework.stereotype.Repository;

import java.math.BigInteger;
import java.util.List;

/**
 * @author al-amin
 * @author nourin
 * @since 12/22/16
 */
@Repository
public class ProductDao extends AbstractDao<Product> {

    private final String CATEGORY = "category";
    private final String AUTHOR = "author";
    private final String PUBLISHER = "publisher";

    public List<Product> getProductsByCategory(long categoryId) {
        return (List<Product>) em.createNativeQuery("SELECT * FROM product WHERE book_id in"
                + " (SELECT book_id FROM book_sub_category WHERE sub_category_id in"
                + " (SELECT id FROM subcategory WHERE category_id=:id))", Product.class)
                .setParameter("id", categoryId)
                .getResultList();
    }

    public List<Product> getProductsByAuthor(long authorId) {
        return (List<Product>) em.createNativeQuery("SELECT * FROM product WHERE book_id in"
                + " (SELECT book_id FROM book_author WHERE author_id=:id)", Product.class)
                .setParameter("id", authorId)
                .getResultList();
    }

    public List<Product> getProductsByPublisher(long publisherId) {
        return (List<Product>) em.createNativeQuery("SELECT * FROM product WHERE book_id in"
                + " (SELECT id FROM book WHERE publisher_id=:id)", Product.class)
                .setParameter("id", publisherId)
                .getResultList();
    }

    public List<Product> getProductsByStores(Store store) {
        return findAllBy("store", store);
    }

    public List<Product> filterBy(ChoiceListCommand choiceListCommand, String specificType, Long specificTypeId) {
        String filterQuery = getFilterQuery(choiceListCommand, specificType);
        List<BigInteger> productIdList = em.createNativeQuery(filterQuery)
                 .getResultList();

        List<Product> productList;
        if(specificType.equals(CATEGORY)) {
            productList = getProductsByCategory(specificTypeId);
        } else if(specificType.equals(AUTHOR)) {
            productList = getProductsByAuthor(specificTypeId);
        } else if(specificType.equals(PUBLISHER)) {
            productList = getProductsByPublisher(specificTypeId);
        } else {
            Store store = new Store();
            store.setId(specificTypeId);
            productList = getProductsByStores(store);
        }
        return applyFilter(productIdList, productList);
    }

    private List<Product> applyFilter(List<BigInteger> idList, List<Product> productList) {

        for(int i = 0; i < productList.size(); i++) {
            Product product = productList.get(i);
            boolean isFound = false;
            for(int j = 0; j < idList.size(); j++) {

                if(idList.get(j).longValue() == product.getId()) {
                    isFound = true;
                    break;
                }
            }
            if(!isFound) {
                productList.remove(i);
            }
        }

        return productList;
    }

    private String getFilterQuery(ChoiceListCommand choiceList, String specificType) {

        StringBuilder query = new StringBuilder("SELECT product.id from product ");
        if(choiceList.getCategoryIds() != null || specificType.equals(CATEGORY)) {
            query.append("JOIN book_sub_category using(book_id) "
                    +"JOIN subcategory ON book_sub_category.sub_category_id = subcategory.id "
                    +"JOIN category ON subcategory.category_id = category.id ");
        }
        if(choiceList.getAuthorIds() != null || specificType.equals(AUTHOR)) {
            query.append("JOIN book_author ON book_author.book_id = product.book_id ");
        }
        if(choiceList.getPublisherIds() != null || specificType.equals(PUBLISHER)) {
            query.append("JOIN book ON product.book_id = book.id ");
        }
        query.append("WHERE ");
        boolean orFlag = false;

        if(choiceList.getAuthorIds() != null) {
            query.append("author_id IN (");
            boolean flag = false;
            for(Long categoryId : choiceList.getAuthorIds()) {
                if(flag){
                    query.append(",");
                }
                query.append(categoryId);
                flag = true;
            }
            query.append(") ");
            orFlag = true;
        }

        if(choiceList.getPublisherIds() != null) {
            if(orFlag) {
                query.append("OR ");
            }
            query.append("publisher_id IN (");
            boolean flag = false;
            for(Long publisherId : choiceList.getPublisherIds()) {
                if(flag){
                    query.append(",");
                }
                query.append(publisherId);
                flag = true;
            }
            query.append(") ");
            orFlag = true;
        }

        if(choiceList.getStoreIds() != null) {
            if(orFlag) {
                query.append("OR ");
            }
            query.append("store_id IN (");
            boolean flag = false;
            for(Long storeId : choiceList.getStoreIds()) {
                if(flag){
                    query.append(",");
                }
                query.append(storeId);
                flag = true;
            }
            query.append(") ");
            orFlag = true;
        }

        if(choiceList.getCategoryIds() != null) {
            if(orFlag) {
                query.append("OR ");
            }
            query.append("category_id IN (");
            boolean flag = false;
            for(Long categoryId : choiceList.getCategoryIds()) {
                if(flag){
                    query.append(",");
                }
                query.append(categoryId);
                flag = true;
            }
            query.append(") ");
        }
        return query.toString();
    }


    public List<Product> findTopProductsPaginated(int page, int size, String fieldName, Object value) {
        return em.createQuery("FROM " + Product.class.getName() +" WHERE " + fieldName + " >= :val")
                .setParameter("val", value)
                .setFirstResult(page * size)
                .setMaxResults(size)
                .getResultList();
    }
}
