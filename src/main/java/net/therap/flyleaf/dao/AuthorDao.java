package net.therap.flyleaf.dao;

import net.therap.flyleaf.domain.Author;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author arafat
 * @since 12/25/16
 */
@Repository
public class AuthorDao extends AbstractDao<Author> {

    public List<Author> getPopularAuthors() {
        String sql = "SELECT * FROM author WHERE id IN(SELECT author_id FROM book_author WHERE book_id IN(SELECT book_id" +
                " FROM product JOIN book_author USING(book_id) WHERE avg_rating >= 3.5) GROUP BY author.name)";

        List<Author> popularAuthors = em.createNativeQuery(sql, Author.class).getResultList();

        return popularAuthors;
    }

    public Author doesExist(String searchKeyword) {
        List<Author> list = em.createQuery("FROM " + Author.class.getName() + " WHERE " + "name" + " " +
                "LIKE :searchKeyword", Author.class)
                .setParameter("searchKeyword", "%" + searchKeyword + "%")
                .getResultList();

        return !list.isEmpty() ? list.get(0) : null;
    }

    public Author addAuthor(Author author) {
        em.persist(author);
        em.flush();

        return author;
    }
}