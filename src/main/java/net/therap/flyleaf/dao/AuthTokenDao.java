package net.therap.flyleaf.dao;

import net.therap.flyleaf.domain.AuthToken;
import org.springframework.stereotype.Repository;

/**
 * @author shafin
 * @since 12/26/16
 */
@Repository
public class AuthTokenDao extends AbstractDao<AuthToken> {
}
