package net.therap.flyleaf.web.controller.admin;

import net.therap.flyleaf.domain.Store;
import net.therap.flyleaf.enumerator.ActivityType;
import net.therap.flyleaf.service.ActivityLogService;
import net.therap.flyleaf.service.admin.AdminProductService;
import net.therap.flyleaf.service.admin.AdminStoreService;
import net.therap.flyleaf.util.URL;
import net.therap.flyleaf.util.View;
import net.therap.flyleaf.web.helper.AuthHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.Locale;

/**
 * @author subrata
 * @since 12/22/16
 */
@Controller
public class AdminStoreController {

    @Autowired
    private AdminStoreService adminStoreService;

    @Autowired
    private AdminProductService adminProductService;

    @Autowired
    ActivityLogService activityLogService;

    @GetMapping(value = URL.ADMIN_STORES)
    public String getStores(Locale locale,
                            Model model) {

        model.addAttribute("storeList", adminStoreService.getStoreList());
        return View.ADMIN_STORES;
    }

    @GetMapping(value = URL.ADMIN_STORE)
    public String getStore(Locale locale,
                           @PathVariable(value = "id") int id,
                           @ModelAttribute Store store,
                           Model model) {

        model.addAttribute("store", adminStoreService.getStore(store));
        model.addAttribute("storeProducts", adminProductService.getProducts(id));
        return View.ADMIN_STORE_DETAILS;
    }

    @PostMapping(value = URL.ADMIN_VERIFY_STORE)
    public String verifyStore(Locale locale,
                              HttpServletRequest request,
                              @PathVariable(value = "id") int id,
                              @ModelAttribute Store store,
                              Model model) {

        adminStoreService.verifyStore(store);
        activityLogService.writeLog(ActivityType.UPDATE, "You have verified an store", new Date()
                , AuthHelper.getUserIdFromSession(request));

        return "redirect:" + URL.ADMIN_STORE;
    }
}