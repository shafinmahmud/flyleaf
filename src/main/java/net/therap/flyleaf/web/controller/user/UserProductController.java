package net.therap.flyleaf.web.controller.user;

import net.therap.flyleaf.service.UserProductService;
import net.therap.flyleaf.service.UserService;
import net.therap.flyleaf.util.URL;
import net.therap.flyleaf.util.View;
import net.therap.flyleaf.web.command.ProductInfo;
import net.therap.flyleaf.web.helper.AuthHelper;
import net.therap.flyleaf.web.helper.UserHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Locale;

/**
 * @author shafin
 * @since 12/29/16
 */
@Controller
public class UserProductController {

    @Autowired
    private UserService userService;

    @Autowired
    private UserProductService userProductService;

    @GetMapping(value = URL.USER_PURCHASES)
    public String showUserPurchases(Locale locale,
                                    HttpServletRequest request,
                                    Model model) {

        long userId = AuthHelper.getUserIdFromSession(request);
        List<ProductInfo> productList = userProductService.getUserPurchasedProducts(userId);
        model.addAttribute("productList", productList);

        UserHelper.bindUserCard(userService, request, model);

        return View.USER_PURCHASES;
    }

    @GetMapping(value = URL.USER_RATINGS)
    public String showUserRatings(Locale locale,
                                  HttpServletRequest request,
                                  Model model) {

        long userId = AuthHelper.getUserIdFromSession(request);
        List<ProductInfo> productList = userProductService.getUserRatedProducts(userId);
        model.addAttribute("ratedProductList", productList);

        UserHelper.bindUserCard(userService, request, model);

        return View.USER_RATINGS;
    }
}
