package net.therap.flyleaf.web.controller.admin;

import net.therap.flyleaf.service.admin.AdminOrderService;
import net.therap.flyleaf.service.admin.AdminProductService;
import net.therap.flyleaf.service.admin.AdminStoreService;
import net.therap.flyleaf.util.URL;
import net.therap.flyleaf.util.View;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.Locale;

/**
 * @author subrata
 * @since 12/22/16
 */
@Controller
public class AdminDashboardController {

    @Autowired
    AdminOrderService adminOrderService;

    @Autowired
    AdminProductService adminProductService;

    @Autowired
    AdminStoreService adminStoreService;

    @GetMapping(value = URL.ADMIN_DASHBOARD)
    public String getDashboardInfo(Locale locale, Model model) {

        model.addAttribute("countPendingOrders", adminOrderService.countPendingOrders());
        model.addAttribute("countProcessingOrders", adminOrderService.countProcessingOrders());
        model.addAttribute("countNotVerifiedProducts", adminProductService.countNotVerifiedProducts());
        model.addAttribute("countPendingStores", adminStoreService.countPendingStores());

        return View.ADMIN_DASHBOARD;
    }
}