package net.therap.flyleaf.web.controller.user;

import net.therap.flyleaf.domain.User;
import net.therap.flyleaf.enumerator.ActivityType;
import net.therap.flyleaf.service.ActivityLogService;
import net.therap.flyleaf.service.AuthorizationService;
import net.therap.flyleaf.service.UserService;
import net.therap.flyleaf.util.URL;
import net.therap.flyleaf.util.View;
import net.therap.flyleaf.web.command.LoginForm;
import net.therap.flyleaf.web.command.SignUpForm;
import net.therap.flyleaf.web.helper.AuthHelper;
import net.therap.flyleaf.web.helper.CookieManager;
import net.therap.flyleaf.web.validator.PasswordValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * @author shafin
 * @since 12/25/16
 */
@Controller
public class AuthController {

    @Autowired
    private UserService userService;

    @Autowired
    private AuthorizationService authService;

    @Autowired
    private ActivityLogService logService;

    @Autowired
    private CookieManager cookieManager;

    private PasswordValidator passwordValidator;

    @Autowired
    public void setValidator(PasswordValidator validator) {
        this.passwordValidator = validator;
    }

    @InitBinder
    private void dateBinder(WebDataBinder binder) {

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        CustomDateEditor editor = new CustomDateEditor(dateFormat, true);
        binder.registerCustomEditor(Date.class, editor);
    }

    @GetMapping(value = {URL.USER_LOGIN, URL.USER_SIGN_UP})
    public String showLoginSignup(Locale locale,
                                  HttpServletRequest request,
                                  Model model) {

        User user = cookieManager.getUserByValidatingCookie(request.getCookies());
        if (user != null) {

            AuthHelper.setSession(request, user);
            return "redirect:" + URL.USER_OVERVIEW;
        }

        AuthHelper.bindLoginFormData(model, new LoginForm());
        AuthHelper.bindSignUpFormData(model, new SignUpForm());

        return View.LOGIN_SIGNUP_VIEW;
    }

    @PostMapping(value = URL.USER_LOGIN)
    public String doLogin(Locale locale,
                          HttpServletRequest request,
                          HttpServletResponse response,
                          @Valid LoginForm loginForm,
                          BindingResult result,
                          Model model) {

        if (!result.hasErrors()) {

            if (authService.isAuthorized(loginForm)) {
                if (loginForm.isRememberMe()) {
                    Cookie cookie = cookieManager.setAuthCookie(loginForm.getEmail());
                    cookie.setPath(request.getContextPath());
                    response.addCookie(cookie);
                }

                User user = userService.getUser(loginForm.getEmail());
                AuthHelper.setSession(request, user);

                logService.insertActivityLog(user.getId(), ActivityType.LOGIN, " Logged In the System.");
                return "redirect:" + URL.USER_OVERVIEW;
            }

            model.addAttribute("loginFail", true);
        }

        AuthHelper.bindLoginFormData(model, loginForm);
        AuthHelper.bindSignUpFormData(model, new SignUpForm());

        return View.LOGIN_SIGNUP_VIEW;
    }

    @GetMapping(value = URL.USER_LOGOUT)
    @PostMapping(value = URL.USER_LOGOUT)
    public String doLogOut(Locale locale,
                           HttpServletRequest request,
                           Model model) {

        long userId = AuthHelper.getUserIdFromSession(request);
        if (userId > 0) {

            request.getSession().invalidate();
            cookieManager.invalidateAuthCookie(userId, request.getCookies());

            logService.insertActivityLog(userId, ActivityType.LOGOUT, " Logged Out the System.");
            return "redirect:" + URL.HOME;
        }

        return View.NOT_FOUND;
    }

    @PostMapping(value = URL.USER_SIGN_UP)
    public String doSignup(Locale locale,
                           HttpServletRequest request,
                           @Valid SignUpForm signUpForm,
                           BindingResult result,
                           Model model) {

        passwordValidator.validate(signUpForm, result);

        if (!result.hasErrors()) {
            userService.signUpUser(signUpForm);
            authService.sendEmailVerificationLink(signUpForm.getEmail());

            model.addAttribute("signUpSuccess", true);
            AuthHelper.bindLoginFormData(model, new LoginForm());
            AuthHelper.bindSignUpFormData(model, new SignUpForm());

            logService.insertActivityLog(userService.getUser(signUpForm.getEmail()).getId(),
                    ActivityType.SIGNUP, " Signed Up in the System.");

            return View.LOGIN_SIGNUP_VIEW;
        }

        AuthHelper.bindLoginFormData(model, new LoginForm());
        AuthHelper.bindSignUpFormData(model, signUpForm);

        return View.LOGIN_SIGNUP_VIEW;
    }

    @GetMapping(value = URL.VERIFY_EMAIL)
    public String acceptVerificationToken(Locale locale,
                                          @PathVariable("email") String email,
                                          @PathVariable("token") String token,
                                          Model model) {

        if (authService.isVerificationTokenValid(email, token)) {
            User user = userService.getUser(email);
            model.addAttribute("user", user.getFirstName() + " " + user.getLastName());

            logService.insertActivityLog(user.getId(), ActivityType.EMAIL_VERIFICATION, " Verified Email.");

            return View.VERIFIED_VIEW;
        }

        return View.NOT_FOUND;
    }

    @GetMapping(value = URL.FORGOT_PASSWORD)
    public String showForgotPassword(Locale locale, Model model) {
        return View.FORGOT_PASSWORD_VIEW;
    }

    @GetMapping(value = URL.RESET_PASSWORD)
    public String showResetPassword(Locale locale, Model model) {
        return View.FORGOT_PASSWORD_VIEW;
    }
}
