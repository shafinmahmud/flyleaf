package net.therap.flyleaf.web.controller.admin;

import net.therap.flyleaf.domain.Category;
import net.therap.flyleaf.domain.SubCategory;
import net.therap.flyleaf.enumerator.ActivityType;
import net.therap.flyleaf.service.ActivityLogService;
import net.therap.flyleaf.service.admin.AdminCategoryService;
import net.therap.flyleaf.util.URL;
import net.therap.flyleaf.util.View;
import net.therap.flyleaf.web.helper.AuthHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.Locale;

/**
 * @author subrata
 * @since 12/22/16
 */
@Controller
public class AdminCategoryController {

    @Autowired
    private AdminCategoryService adminCategoryService;

    @Autowired
    private ActivityLogService activityLogService;

    @GetMapping(value = URL.ADMIN_CATEGORIES)
    public String getCategories(Locale locale, Model model) {
        model.addAttribute("categories", adminCategoryService.getCategories());
        return View.ADMIN_PRODUCT_CATEGORY;
    }

    @PostMapping(value = URL.ADMIN_ADD_CATEGORY)
    public String addCategory(Locale locale,
                              HttpServletRequest request,
                              @ModelAttribute Category category,
                              Model model) {

        adminCategoryService.addCategory(category);
        activityLogService.writeLog(ActivityType.INSERT, "Inserted A Category", new Date(),
                AuthHelper.getUserIdFromSession(request));

        return "redirect:" + URL.ADMIN_CATEGORIES;
    }

    @PostMapping(value = URL.ADMIN_UPDATE_CATEGORY)
    public String updateCategory(Locale locale,
                                 HttpServletRequest request,
                                 @ModelAttribute Category category,
                                 Model model) {

        adminCategoryService.updateCategory(category);
        activityLogService.writeLog(ActivityType.UPDATE, "Updated A Category", new Date(),
                AuthHelper.getUserIdFromSession(request));

        return "redirect:" + URL.ADMIN_CATEGORIES;
    }

    @GetMapping(value = URL.ADMIN_SHOW_SUBCATEGORIES)
    public String getCategory(Locale locale,
                              @PathVariable(value = "categoryId") long id,
                              Model model) {

        model.addAttribute("categoryId", id);
        model.addAttribute("category", adminCategoryService.getCategory(id));
        return View.ADMIN_PRODUCT_SUBCATEGORY;
    }

    @PostMapping(value = URL.ADMIN_ADD_SUBCATEGORY)
    public String addSubCategory(Locale locale,
                                 HttpServletRequest request,
                                 @PathVariable(value = "categoryId") int id,
                                 @ModelAttribute SubCategory subCategory,
                                 Model model) {

        adminCategoryService.addSubCategory(id, subCategory);
        activityLogService.writeLog(ActivityType.INSERT, "Inserted A SubCategory", new Date(),
                AuthHelper.getUserIdFromSession(request));

        return "redirect:" + URL.ADMIN_SHOW_SUBCATEGORIES.replace("{id}", id + "");
    }

    @PostMapping(value = URL.ADMIN_UPDATE_SUBCATEGORY)
    public String updateSubCategory(Locale locale,
                                    HttpServletRequest request,
                                    @PathVariable(value = "categoryId") int id,
                                    @ModelAttribute SubCategory subCategory,
                                    Model model) {

        adminCategoryService.updateSubCategory(subCategory);
        activityLogService.writeLog(ActivityType.UPDATE, "Updated A SubCategory", new Date(),
                AuthHelper.getUserIdFromSession(request));

        return "redirect:" + URL.ADMIN_SHOW_SUBCATEGORIES.replace("{id}", id + "");
    }
}