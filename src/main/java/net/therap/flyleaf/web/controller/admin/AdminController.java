package net.therap.flyleaf.web.controller.admin;

import net.therap.flyleaf.domain.User;
import net.therap.flyleaf.enumerator.ActivityType;
import net.therap.flyleaf.enumerator.RoleType;
import net.therap.flyleaf.service.ActivityLogService;
import net.therap.flyleaf.service.AuthorizationService;
import net.therap.flyleaf.service.UserService;
import net.therap.flyleaf.util.URL;
import net.therap.flyleaf.util.View;
import net.therap.flyleaf.web.command.LoginForm;
import net.therap.flyleaf.web.helper.AuthHelper;
import net.therap.flyleaf.web.helper.CookieManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.Date;
import java.util.Locale;

/**
 * @author subrata
 * @author shafin
 * @since 12/22/16
 */
@Controller
public class AdminController {

    @Autowired
    private UserService userService;

    @Autowired
    private AuthorizationService authService;

    @Autowired
    ActivityLogService activityLogService;

    @Autowired
    CookieManager cookieManager;

    @GetMapping(value = URL.ADMIN_LOGIN)
    public String showLoginSignup(Locale locale,
                                  HttpServletRequest request,
                                  Model model) {

        User user = cookieManager.getUserByValidatingCookie(request.getCookies());
        if (user != null) {
            AuthHelper.setSession(request, user);
            return "redirect:" + URL.ADMIN_DASHBOARD;
        }

        AuthHelper.bindLoginFormData(model, new LoginForm());
        return View.ADMIN_LOGIN;
    }

    @PostMapping(value = URL.ADMIN_LOGIN)
    public String doLogin(Locale locale,
                          HttpServletRequest request,
                          HttpServletResponse response,
                          @Valid LoginForm loginForm,
                          BindingResult result,
                          Model model) {

        if (!result.hasErrors()) {

            if (authService.isAuthorized(loginForm)) {
                if (loginForm.isRememberMe()) {
                    Cookie cookie = cookieManager.setAuthCookie(loginForm.getEmail());
                    cookie.setPath(request.getContextPath());
                    response.addCookie(cookie);
                }

                User user = userService.getUser(loginForm.getEmail());

                if (AuthHelper.isUserHavingRole(user, RoleType.ADMIN)) {
                    AuthHelper.setSession(request, user);
                    activityLogService.writeLog(ActivityType.LOGIN, "You have logged in to your account", new Date()
                            , AuthHelper.getUserIdFromSession(request));
                    return "redirect:" + URL.ADMIN_DASHBOARD;
                }
            }

            model.addAttribute("loginFail", true);
        }

        AuthHelper.bindLoginFormData(model, loginForm);
        return View.ADMIN_LOGIN;
    }

    @PostMapping(value = URL.ADMIN_LOGOUT)
    public String doLogOut(Locale locale, HttpServletRequest request, Model model) {

        long userId = AuthHelper.getUserIdFromSession(request);
        if (userId > 0) {
            request.getSession().invalidate();
            cookieManager.invalidateAuthCookie(userId, request.getCookies());
        }

        activityLogService.writeLog(ActivityType.LOGOUT, "You have logged out to your account", new Date()
                , AuthHelper.getUserIdFromSession(request));
        return "redirect:" + URL.ADMIN_LOGIN;
    }
}