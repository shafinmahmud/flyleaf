package net.therap.flyleaf.web.controller;

import net.therap.flyleaf.domain.*;
import net.therap.flyleaf.service.AuthorService;
import net.therap.flyleaf.service.admin.AdminCategoryService;
import net.therap.flyleaf.service.admin.AdminStoreService;
import net.therap.flyleaf.service.product.CustomerProductService;
import net.therap.flyleaf.service.publisher.PublisherService;
import net.therap.flyleaf.web.command.ChoiceListCommand;
import net.therap.flyleaf.web.command.ProductCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * @author nourin
 * @since 12/28/16
 */

@Controller
public class ProductViewController {

    @Autowired
    private CustomerProductService customerProductService;

    @Autowired
    private AdminCategoryService categoryService;

    @Autowired
    private AuthorService authorService;

    @Autowired
    private PublisherService publisherService;

    @Autowired
    private AdminStoreService storeService;

    private final String CATEGORY = "category";
    private final String AUTHOR = "author";
    private final String PUBLISHER = "publisher";
    private final String STORE = "store";

    @GetMapping(value = "/product/category/{id}")
    public String getProductsByCategory(Model model, @PathVariable(value = "id") long categoryId, HttpServletRequest request) {

        model.addAttribute("productList", customerProductService.getProductsbyCategory(categoryId));
        addCommonAttributes(model, CATEGORY, categoryId);
        return "books";
    }

    @GetMapping(value = "/product/author/{id}")
    public String getProductsByAuthor(Model model, @PathVariable(value = "id") long authorId) {

        model.addAttribute("productList", customerProductService.getProductsByAuthor(authorId));
        addCommonAttributes(model, AUTHOR, authorId);
        return "books";
    }

    @GetMapping(value = "/product/publisher/{id}")
    public String getProductsByPublisher(Model model, @PathVariable(value = "id") long publisherId) {

        model.addAttribute("productList", customerProductService.getProductsbyPublisher(publisherId));
        addCommonAttributes(model, PUBLISHER, publisherId);
        return "books";
    }

    @GetMapping(value = "/product/store/{id}")
    public String getProductsByStore(Model model, @PathVariable(value = "id") long storeId) {

        model.addAttribute("productList", customerProductService.getProductsByStore(storeId));
        addCommonAttributes(model, STORE, storeId);
        return "books";
    }

    private void addCommonAttributes(Model model, String choiceExcluded, long choiceTypeId) {

        if (!choiceExcluded.equals(CATEGORY)) {
            List<Category> categoryList = categoryService.getCategories();
            model.addAttribute("categoryList", categoryList);
        }
        if (!choiceExcluded.equals(AUTHOR)) {
            List<Author> authorList = authorService.getAuthors();
            model.addAttribute("authorList", authorList);
        }
        if (!choiceExcluded.equals(PUBLISHER)) {
            List<Publisher> publisherList = publisherService.getPublishers();
            model.addAttribute("publisherList", publisherList);
        }
        if (!choiceExcluded.equals(STORE)) {
            List<Store> storeList = storeService.getStoreList();
            model.addAttribute("storeList", storeList);
        }
        model.addAttribute("filterExcluded", choiceExcluded);
        model.addAttribute("choiceTypeId", choiceTypeId);
    }

    private void addCommonAttributes(Model model, String choiceExcluded, long choiceTypeId,
                                     List<Long> authorIds,
                                     List<Long> categoryIds,
                                     List<Long> publisherIds,
                                     List<Long> storeIds) {

        if (!choiceExcluded.equals(CATEGORY)) {
            List<Category> categoryList = categoryService.getCategories();
            setCheckedCategory(categoryList, categoryIds);
            model.addAttribute("categoryList", categoryList);
        }
        if (!choiceExcluded.equals(AUTHOR)) {
            List<Author> authorList = authorService.getAuthors();
            setCheckedAuthor(authorList, authorIds);
            model.addAttribute("authorList", authorList);
        }
        if (!choiceExcluded.equals(PUBLISHER)) {
            List<Publisher> publisherList = publisherService.getPublishers();
            setCheckedPublisher(publisherList, publisherIds);
            model.addAttribute("publisherList", publisherList);
        }
        if (!choiceExcluded.equals(STORE)) {
            List<Store> storeList = storeService.getStoreList();
            setCheckedStore(storeList, storeIds);
            model.addAttribute("storeList", storeList);
        }
        model.addAttribute("filterExcluded", choiceExcluded);
        model.addAttribute("choiceTypeId", choiceTypeId);
    }

    private List<Category> setCheckedCategory(List<Category> categoryList, List<Long> categoryIds) {
        if (categoryIds == null) return categoryList;
        for (Category category : categoryList) {
            if (categoryIds.contains(category.getId())) {
                category.setChecked(true);
            } else {
                category.setChecked(false);
            }
        }
        return categoryList;
    }

    private List<Author> setCheckedAuthor(List<Author> authorList, List<Long> authorIds) {
        if (authorIds == null) return authorList;
        for (Author author : authorList) {
            if (authorIds.contains(author.getId())) {
                author.setChecked(true);
            } else {
                author.setChecked(false);
            }
        }
        return authorList;
    }

    private List<Publisher> setCheckedPublisher(List<Publisher> publisherList, List<Long> publisherIds) {
        if (publisherIds == null) return publisherList;
        for (Publisher publisher : publisherList) {
            if (publisherIds.contains(publisher.getId())) {
                publisher.setChecked(true);
            } else {
                publisher.setChecked(false);
            }
        }
        return publisherList;
    }

    private List<Store> setCheckedStore(List<Store> storeList, List<Long> storeIds) {
        if (storeIds == null) return storeList;
        for (Store store : storeList) {
            if (storeIds.contains(store.getId())) {
                store.setChecked(true);
            } else {
                store.setChecked(false);
            }
        }
        return storeList;
    }

    @GetMapping(value = "/product")
    public String getProductsbyFiltering(Model model,
                                         @RequestParam(required = false) List<Long> authorIds,
                                         @RequestParam(required = false) List<Long> categoryIds,
                                         @RequestParam(required = false) List<Long> publisherIds,
                                         @RequestParam(required = false) List<Long> storeIds,
                                         @RequestParam String filterExcluded,
                                         @RequestParam Long choiceTypeId) {

        String pageToLoad = "books";
        if (authorIds == null && categoryIds == null && publisherIds == null && storeIds == null) {
            pageToLoad = "redirect:/product/" + filterExcluded + "/" + choiceTypeId;
        } else {
            ChoiceListCommand choiceListCommand = new ChoiceListCommand();
            choiceListCommand.setAuthorIds(authorIds);
            choiceListCommand.setCategoryIds(categoryIds);
            choiceListCommand.setPublisherIds(publisherIds);
            choiceListCommand.setStoreIds(storeIds);

            List<Product> productList =
                    customerProductService.getFilteredProducts(choiceListCommand, filterExcluded, choiceTypeId);
            model.addAttribute("productList", productList);
            addCommonAttributes(model, filterExcluded, choiceTypeId, authorIds, categoryIds, publisherIds, storeIds);
        }

        return pageToLoad;
    }

    @GetMapping(value = "/product/{id}")
    public String showSingleProduct(Model model, @PathVariable(value = "id") long id) {

        ProductCommand productCommand = new ProductCommand();

        Product product = customerProductService.getProduct(id);
        productCommand.setProduct(product);
        productCommand.setCategory(customerProductService.getCategory(product));
        productCommand.setAuthors(getCommaSeperatedAuthorFromAuthorList(customerProductService.getAuthor(product)));
        productCommand.setCurrentPrice(calculateAndGetCurrentPrice(product));

        productCommand.setListOfProductsFromSimilarCategory(getProductsFromSimilarCategory(product));
        productCommand.setListOfProductsFromSimilarPublisher(getProductsFromSimilarPublisher(product));
        productCommand.setListOfProductsFromSimilarStore(getProductsFromSimilarStore(product));
        productCommand.setListOfProductsFromSimilarAuthor(getProductsFromSimilarAuthor(product));

        model.addAttribute("singleProduct", productCommand);

        return "bookDetails";
    }

    private double calculateAndGetCurrentPrice(Product product) {
        if (product.getDiscount() == 0.0) {
            return product.getBasePrice();
        } else {
            return product.getBasePrice() - (product.getBasePrice() * (product.getDiscount() / 100.0));
        }
    }

    @GetMapping(value = "/product/rating")
    public String productRating(Model model, @RequestParam("productId") long productId, @RequestParam("rating") String[] rating) {

        Product product = customerProductService.getProduct(productId);

        UserProductRating userProductRating = new UserProductRating();
        //userProductRating.setProduct();

        return "redirect:/product/" + productId;
    }

    private String getCommaSeperatedAuthorFromAuthorList(List<Author> authorList) {
        String authorsSeperatedByComma = "";

        for (int i = 0; i < authorList.size(); i++) {
            if (i <= authorList.size() - 2) {
                authorsSeperatedByComma += authorList.get(i).getName() + ", ";
            } else {
                authorsSeperatedByComma += authorList.get(i).getName();
            }
        }

        return authorsSeperatedByComma;
    }

    private List<Product> getProductsFromSimilarAuthor(Product product) {

        HashMap<Author, List<Product>> hashMapOfAuthorToProducts = getMapOfProductsFromSimilarAuthor(product);
        List<Product> listOfProducts = new ArrayList<>();

        List<String> titleList = new ArrayList<>();

        for (Author author : hashMapOfAuthorToProducts.keySet()) {
            for (Product p : hashMapOfAuthorToProducts.get(author)) {
                if (!titleList.contains(p.getBook().getTitle())) {
                    listOfProducts.add(p);
                    titleList.add(p.getBook().getTitle());
                }
            }
        }

        return getTopFour(listOfProducts);
    }

    private HashMap<Author, List<Product>> getMapOfProductsFromSimilarAuthor(Product product) {

        HashMap<Author, List<Product>> productList = new HashMap<>();

        for (Author author : customerProductService.getAuthor(product)) {
            productList.put(author, customerProductService.getProductsByAuthor(author.getId()));
        }

        return productList;
    }

    private List<Product> getProductsFromSimilarCategory(Product product) {
        return getTopFour(customerProductService.getProductsbyCategory
                (customerProductService.getCategory(product).getId()));
    }

    private List<Product> getProductsFromSimilarPublisher(Product product) {
        return getTopFour(customerProductService.getProductsbyPublisher
                (customerProductService.getPublisher(product).getId()));
    }

    private List<Product> getProductsFromSimilarStore(Product product) {
        return getTopFour(customerProductService.getProductsbyStore(customerProductService.getStore(product)));
    }

    private List<Product> getTopFour(List<Product> lisOfAllProducts) {
        if (lisOfAllProducts.size() <= 4) {
            return lisOfAllProducts;
        } else {
            return lisOfAllProducts.subList(0, 4);
        }
    }
}
