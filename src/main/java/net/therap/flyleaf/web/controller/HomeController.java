package net.therap.flyleaf.web.controller;

import net.therap.flyleaf.domain.Product;
import net.therap.flyleaf.service.category.UserCategoryService;
import net.therap.flyleaf.service.product.CustomerProductService;
import net.therap.flyleaf.util.Constant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author shafin
 * @author arafat
 * @since 12/20/16
 */
@Controller
public class HomeController {

    @Autowired
    private UserCategoryService userCategoryService;

    @Autowired
    private CustomerProductService productService;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String home(Model model, HttpServletRequest request) {

        int pageNumber = 1;
        int pageSize = 6;

        if (request.getParameter("page") != null) {
            pageNumber = Integer.parseInt(request.getParameter("page"));
        }

        List<Product> topProducts = productService.getProductsByTopRating(Constant.AVERAGE_RATING,
                Constant.THRESHOLD_RATING);

        long recordsCount = productService.getProductsByTopRating(Constant.AVERAGE_RATING,
                Constant.THRESHOLD_RATING).size();

        long noOfPages = (int) Math.ceil(recordsCount * 1.0 / pageSize);

        List<Product> paginatedTopProductList = productService.getProducts(pageNumber - 1, pageSize, Constant.AVERAGE_RATING
                , Constant.THRESHOLD_RATING);

        model.addAttribute("noOfPages", noOfPages);
        model.addAttribute("currentPage", pageNumber);
        model.addAttribute("popularObjectList", userCategoryService.getPopularCategories());
        model.addAttribute("topRatedBookList", paginatedTopProductList);
        model.addAttribute("topRatedProductListForCarousal",
                topProducts.subList(0, Math.min(topProducts.size(), 4)));

        return "home";
    }

    @RequestMapping(value = "/search", method = RequestMethod.GET)
    public String showSearchResult(@RequestParam("property") String property,
                                   @RequestParam("searchKeyWord") String searchKeyWord, Model model) {
        String pageToLoad;
        if (searchKeyWord.equals("")) {
            pageToLoad = "redirect:/home";

        } else {
            model.addAttribute("productList", productService.getSearchResult(property,
                    formatSearchKeyWord(searchKeyWord)));

            pageToLoad = "books";
        }

        return pageToLoad;
    }

    private String formatSearchKeyWord(String searchKeyWord) {
        return searchKeyWord.substring(0, 1).toUpperCase() + searchKeyWord.substring(1).toLowerCase();
    }
}