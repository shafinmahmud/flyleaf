package net.therap.flyleaf.web.controller.admin;

import net.therap.flyleaf.domain.Product;
import net.therap.flyleaf.enumerator.ActivityType;
import net.therap.flyleaf.service.ActivityLogService;
import net.therap.flyleaf.service.admin.AdminProductService;
import net.therap.flyleaf.util.URL;
import net.therap.flyleaf.web.helper.AuthHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.Locale;

/**
 * @author subrata
 * @since 12/22/16
 */
@Controller
public class AdminProductVerificationController {

    @Autowired
    private AdminProductService adminProductService;

    @Autowired
    private ActivityLogService activityLogService;

    @GetMapping(value = URL.ADMIN_NEW_PRODUCTS)
    public String getNewProducts(Locale locale, Model model) {

        model.addAttribute("newProductList", adminProductService.getNewProducts());
        return URL.ADMIN_NEW_PRODUCTS;
    }

    @PostMapping(value = URL.ADMIN_VERIFY_NEW_PRODUCT)
    public String verifyProduct(Locale locale, HttpServletRequest request,
                                @ModelAttribute Product product, Model model) {

        adminProductService.verifyNewProduct(product);
        activityLogService.writeLog(ActivityType.UPDATE, "You have verified a product", new Date()
                , AuthHelper.getUserIdFromSession(request));

        return URL.ADMIN_NEW_PRODUCTS;
    }
}