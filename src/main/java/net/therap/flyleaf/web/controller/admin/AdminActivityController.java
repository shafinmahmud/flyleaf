package net.therap.flyleaf.web.controller.admin;

import net.therap.flyleaf.domain.User;
import net.therap.flyleaf.service.UserService;
import net.therap.flyleaf.service.ActivityLogService;
import net.therap.flyleaf.util.URL;
import net.therap.flyleaf.util.View;
import net.therap.flyleaf.web.helper.AuthHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import javax.servlet.http.HttpServletRequest;
import java.util.Locale;

/**
 * @author subrata
 * @since 12/22/16
 */
@Controller
public class AdminActivityController {

    @Autowired
    private ActivityLogService activityLogService;

    @Autowired
    private UserService userService;

    @GetMapping(value = URL.ADMIN_ACTIVITY)
    public String getActivityLog(Locale locale,
                                 HttpServletRequest request,
                                 @PathVariable(value = "page") int page,
                                 Model model) {

        long userId = AuthHelper.getUserIdFromSession(request);
        int resultSize = 10;

        User user = userService.getUser(userId);
        model.addAttribute("activityLog", activityLogService.getActivityLog(user, page, resultSize));
        model.addAttribute("countNextLogs", activityLogService.countNextLogs(user, page + 1, resultSize));
        model.addAttribute("page", page);

        return View.ADMIN_ACTIVITY;
    }
}