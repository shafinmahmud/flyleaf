package net.therap.flyleaf.web.controller.storekeeper;

import net.therap.flyleaf.domain.User;
import net.therap.flyleaf.service.ActivityLogService;
import net.therap.flyleaf.service.storekeeper.MyUserService;
import net.therap.flyleaf.web.helper.AuthHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import javax.servlet.http.HttpServletRequest;
import java.util.Locale;

/**
 * @author Al-Amin
 * @since 12/29/2016.
 */
@Controller
public class MyActivityLogController {

    @Autowired
    private ActivityLogService activityLogService;

    @Autowired
    private MyUserService myUserService;

    @GetMapping(value = "/mystores/activity/{page}")
    public String getActivityLog(Locale locale,
                                 HttpServletRequest request,
                                 @PathVariable(value = "page") int page,
                                 Model model) {

        long userId = AuthHelper.getUserIdFromSession(request);
        int resultSize = 10;

        User user = myUserService.getUser(userId);
        model.addAttribute("activityLog", activityLogService.getMyActivityLog(user, page, resultSize));
        model.addAttribute("countNextLogs", activityLogService.countMyNextLogs(user, page + 1, resultSize));
        model.addAttribute("page", page);

        return "store/activityLog";
    }
}
