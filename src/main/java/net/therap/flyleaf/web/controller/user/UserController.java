package net.therap.flyleaf.web.controller.user;

import net.therap.flyleaf.enumerator.ActivityType;
import net.therap.flyleaf.service.ActivityLogService;
import net.therap.flyleaf.service.UserOrderService;
import net.therap.flyleaf.service.UserProductRatingService;
import net.therap.flyleaf.service.UserService;
import net.therap.flyleaf.service.cart.CartService;
import net.therap.flyleaf.util.URL;
import net.therap.flyleaf.util.View;
import net.therap.flyleaf.web.command.UserProfileForm;
import net.therap.flyleaf.web.helper.AuthHelper;
import net.therap.flyleaf.web.helper.UserHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Locale;

/**
 * @author shafin
 * @since 12/26/16
 */
@Controller
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private UserOrderService userOrderService;

    @Autowired
    private UserProductRatingService ratingService;

    @Autowired
    private CartService cartService;

    @Autowired
    private ActivityLogService logService;

    @GetMapping(value = URL.USER_OVERVIEW)
    public String showUserHome(Locale locale,
                               HttpServletRequest request,
                               Model model) {

        long userId = AuthHelper.getUserIdFromSession(request);

        model.addAttribute("cartCount", cartService.getUserCartItemCount(userId));
        model.addAttribute("reviewCount", ratingService.getUserProductRatingCount(userId));
        model.addAttribute("purchaseCount", 2);
        model.addAttribute("recentOrder", userOrderService.getMostRecentOrderInfo(userId));
        UserHelper.bindUserCard(userService, request, model);

        return View.USER_HOME;
    }

    @GetMapping(value = URL.USER_PROFILE)
    public String showUserProfile(Locale locale,
                                  HttpServletRequest request,
                                  Model model) {

        UserHelper.bindUserProfile(userService, request, model);
        UserHelper.bindUserCard(userService, request, model);

        return View.USER_PROFILE;
    }

    @GetMapping(value = URL.USER_PROFILE_EDIT)
    public String showUserProfileEdit(Locale locale,
                                      HttpServletRequest request,
                                      Model model) {

        UserHelper.bindUserProfile(userService, request, model);
        UserHelper.bindUserCard(userService, request, model);

        return View.USER_PROFILE_EDIT;
    }

    @PostMapping(value = URL.USER_PROFILE_EDIT)
    public String doUserProfileEdit(Locale locale,
                                    @Valid UserProfileForm userProfileForm,
                                    BindingResult result,
                                    HttpServletRequest request,
                                    Model model) {

        if (!result.hasErrors()) {
            long userId = AuthHelper.getUserIdFromSession(request);
            userService.updateUser(userId, userProfileForm);

            model.addAttribute("profileEditSuccess", true);

            UserHelper.bindUserProfile(userService, request, model);
            UserHelper.bindUserCard(userService, request, model);

            logService.insertActivityLog(userId, ActivityType.PROFILE_UPDATE, " Updated Profile.");

            return View.USER_PROFILE;
        }

        model.addAttribute("userProfileForm", userProfileForm);
        UserHelper.bindUserCard(userService, request, model);

        return View.USER_PROFILE_EDIT;
    }

    @GetMapping(value = URL.USER_ACTIVITY_LOG)
    public String showUserActivityLog(Locale locale,
                                      HttpServletRequest request,
                                      Model model) {

        long userId = AuthHelper.getUserIdFromSession(request);
        model.addAttribute("logList", logService.getUserActivityLog(userId, 0, 10));
        UserHelper.bindUserCard(userService, request, model);

        return View.USER_ACTIVITY_LOG;
    }
}
