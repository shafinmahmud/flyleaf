package net.therap.flyleaf.web.controller.user;

import net.therap.flyleaf.service.UserOrderService;
import net.therap.flyleaf.service.UserService;
import net.therap.flyleaf.util.URL;
import net.therap.flyleaf.util.View;
import net.therap.flyleaf.web.command.OrderInfo;
import net.therap.flyleaf.web.helper.AuthHelper;
import net.therap.flyleaf.web.helper.UserHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Locale;

/**
 * @author shafin
 * @since 12/28/2016
 */
@Controller
public class UserOrderController {

    @Autowired
    private UserService userService;

    @Autowired
    private UserOrderService userOrderService;

    @GetMapping(value = URL.USER_ORDERS)
    public String showUserOrders(Locale locale,
                                 HttpServletRequest request,
                                 @PathVariable int page,
                                 @PathVariable int size,
                                 Model model) {

        long userId = AuthHelper.getUserIdFromSession(request);
        List<OrderInfo> orderInfoList = userOrderService.getUserOrderInfoList(userId, page, size);

        model.addAttribute("currentPage", page);
        model.addAttribute("noOfPages", (userOrderService.getTotalOrderCount(userId) / size));
        model.addAttribute("orderList", orderInfoList);
        model.addAttribute("size", size);
        UserHelper.bindUserCard(userService, request, model);

        return View.USER_ORDERS;
    }

    @GetMapping(value = URL.USER_ORDER_INVOICE)
    public String showUserOrderInvoice(Locale locale,
                                       HttpServletRequest request,
                                       @PathVariable long orderId,
                                       Model model) {

        long userId = AuthHelper.getUserIdFromSession(request);
        OrderInfo orderInfo = userOrderService.getUserOrderInfo(userId, orderId);
        if (orderInfo != null) {

            model.addAttribute("order", orderInfo);
            UserHelper.bindUserCard(userService, request, model);

            return View.USER_ORDER_INVOICE;
        }

        return View.NOT_FOUND;
    }
}
