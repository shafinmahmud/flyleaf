package net.therap.flyleaf.web.controller.storekeeper;

import net.therap.flyleaf.domain.User;
import net.therap.flyleaf.service.storekeeper.MyOrderService;
import net.therap.flyleaf.service.storekeeper.MyUserService;
import net.therap.flyleaf.web.helper.AuthHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Locale;

/**
 * @author al-amin
 * @since 12/22/16
 */
@Controller
public class MyOrderController {

    @Autowired
    private MyUserService myUserService;

    @Autowired
    private MyOrderService myOrderService;

    @GetMapping(value = "/mystores/orders")
    public String getOrderStatus(Locale locale, HttpSession session) {
        return "store/myOrder";
    }

    @GetMapping(value = "/mystores/salesreport")
    public String getSalesReport(Locale locale, HttpSession session) {
        return "store/salesReport";
    }


    @GetMapping(value = "/mystores/order/{page}")
    public String getMyOrders(Locale locale,
                              HttpServletRequest request,
                              @PathVariable(value = "page") int page,
                              Model model) {

        long userId = AuthHelper.getUserIdFromSession(request);
        int resultSize = 10;

        User user = myUserService.getUser(userId);
        model.addAttribute("orderList", myOrderService.getMyOrders(user, page, resultSize));
        model.addAttribute("countMyNextOrders", myOrderService.countMyNextOrders(user, page + 1, resultSize));
        model.addAttribute("page", page);

        return "store/myOrder";
    }
}
