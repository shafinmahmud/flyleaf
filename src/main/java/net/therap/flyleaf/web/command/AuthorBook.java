package net.therap.flyleaf.web.command;

import net.therap.flyleaf.domain.Author;
import net.therap.flyleaf.domain.Book;

import java.util.List;

/**
 * @author Al-Amin
 * @since 12/27/2016.
 */
public class AuthorBook {

    private List<Author> authorList;
    private Book book;

    public List<Author> getAuthorList() {
        return authorList;
    }

    public void setAuthorList(List<Author> authorList) {
        this.authorList = authorList;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }
}
