package net.therap.flyleaf.web.command;

import net.therap.flyleaf.domain.Author;
import net.therap.flyleaf.domain.Book;
import net.therap.flyleaf.domain.Product;

/**
 * @author al-amin
 * @since 12/26/16
 */
public class BookProduct {

    private Book book;
    private Product product;
    private Author author;

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }
}
