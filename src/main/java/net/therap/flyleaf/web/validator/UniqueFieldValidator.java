package net.therap.flyleaf.web.validator;

import net.therap.flyleaf.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * @author shafin
 * @since 12/25/16
 */
@Component
public class UniqueFieldValidator implements ConstraintValidator<UniqueField, Object> {

    @Autowired
    private UserService userService;

    private String fieldName;

    @Override
    public void initialize(UniqueField constraintAnnotation) {
        fieldName = constraintAnnotation.fieldName();
    }

    @Override
    public boolean isValid(Object value, ConstraintValidatorContext context) {

        switch (fieldName) {
            case "email":
                return userService.getUser((String) value) == null;
            default:
                return false;
        }
    }
}
