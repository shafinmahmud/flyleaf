package net.therap.flyleaf.web.helper;

import net.therap.flyleaf.domain.User;
import net.therap.flyleaf.service.UserService;
import net.therap.flyleaf.util.DateUtil;
import net.therap.flyleaf.web.command.UserCard;
import net.therap.flyleaf.web.command.UserProfileForm;
import org.springframework.ui.Model;

import javax.servlet.http.HttpServletRequest;

/**
 * @author shafin
 * @since 12/25/16
 */
public class UserHelper {

    public static void bindUserCard(UserService userService, HttpServletRequest request, Model model) {
        long userId = AuthHelper.getUserIdFromSession(request);
        User user = userService.getUser(userId);

        UserCard userCard = new UserCard();
        userCard.setName(user.getFirstName() + " " + user.getLastName());
        userCard.setEmail(user.getEmail());
        userCard.setMemberSince(DateUtil.formatDate(user.getCreatedAt(), "dd, MMM, YYYY"));

        model.addAttribute("userCard", userCard);
    }

    public static void bindUserProfile(UserService userService, HttpServletRequest request, Model model) {
        long userId = AuthHelper.getUserIdFromSession(request);
        User user = userService.getUser(userId);

        UserProfileForm profile = new UserProfileForm();
        profile.setFirstName(user.getFirstName());
        profile.setLastName(user.getLastName());
        profile.setEmail(user.getEmail());
        profile.setGender(user.getGender());
        profile.setBio(user.getBio());
        profile.setAddress(user.getAddress());
        profile.setDateOfBirth(user.getDateOfBirth());
        profile.setPhone(user.getPhone());
        profile.setImageUrl(user.getImageUrl());

        model.addAttribute("userProfileForm", profile);
    }
}
