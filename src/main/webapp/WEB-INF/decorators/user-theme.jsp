<!DOCTYPE html>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator" %>

<%--
  @author shafin
  @since 12/25/16 4:22 PM
--%>

<html>
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">

    <title>Flyleaf&nbsp;::&nbsp;<decorator:title default="Welcome!"/></title>

    <link rel="brand icon" href="<c:url value='/resources/images/icon/favicon.ico'/>" type="image/x-icon">
    <link href="<c:url value='/resources/lib/bootstrap/css/bootstrap.min.css'/>" rel="stylesheet" type="text/css"/>
    <link href="<c:url value='/resources/css/user-style.css'/>" rel="stylesheet" type="text/css"/>
    <link href="<c:url value='/resources/css/shadow.css'/>" rel="stylesheet" type="text/css"/>
    <link href="<c:url value='/resources/css/alert.css'/>" rel="stylesheet" type="text/css"/>
</head>
<body>

<jsp:include page="user-header.jsp"/>

<hr/>
<decorator:body/>
<hr/>

<jsp:include page="user-footer.jsp"/>

<script src="<c:url value="/resources/lib/jquery-3.1.1.min.js" />"></script>
<script src="<c:url value="/resources/lib/bootstrap/js/bootstrap.min.js" />"></script>
</body>
</html>
