<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<%--
  @author shafin
  @since 5/10/18 4:22 PM
--%>

<nav class="navbar navbar-default navbar-default" style="border-radius:0px !important; margin-bottom:0px;">
    <div class="col-md-2"></div>
    <div class="col-md-8">
        <div class="container-fluid">

            <div class="navbar-header navbar-middle">
                <a class="navbar-brand navbar-right" href="<c:url value="/"/> ">FlyLeaf</a>
            </div>

            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav"></ul>

                <ul class="nav navbar-nav navbar-right">

                    <c:if test="${USER_ID != null}">
                        <li><a href="<c:url value="/user/logout"/> ">Logout</a></li>
                    </c:if>

                    <c:if test="${USER_ID == null}">
                        <li><a href="<c:url value="/user/login"/> ">Login | Register</a></li>
                    </c:if>

                    <c:if test="${fn:endsWith(pageContext.response.locale,'bn')}">
                        <li><a href="<c:url value="?locale=en" />">English</a></li>
                    </c:if>

                    <c:if test="${fn:endsWith(pageContext.response.locale,'en')}">
                        <li><a href="<c:url value="?locale=bn" />">Bangla</a></li>
                    </c:if>
                </ul>

                <ul class="nav navbar-right">
                    <a class="btn btn-danger navbar-btn" href="<c:url value="/mystores"/>"> Your Store</a>
                </ul>

                <div class="form-group has-feedback">
                    <form class="navbar-form navbar-right">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Search Here"/>
                            <i class="glyphicon glyphicon-search form-control-feedback"></i>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-2"></div>
</nav>