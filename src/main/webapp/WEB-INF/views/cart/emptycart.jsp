<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<html>
<head>
    <title>Fly Leaf :: 404</title>
    <link href="<c:url value="/resources/lib/bootstrap/css/bootstrap.min.css"/>" rel="stylesheet"/>
    <link href="<c:url value="/resources/css/style.css" />" rel="stylesheet" type="text/css"/>
</head>
<body>

<div class="container">
    <div class="row">
        <div align="center" class="error-template">
            <h1> Your cart is empty !!!</h1>
            <img src="https://chillydraji.files.wordpress.com/2015/08/empty_cart.jpeg">

            <div class="error-actions">
                <a href="<c:url value="/" />" class="btn btn-primary btn-outline">
                    <i class="icon-home icon-white"></i>Continue Shopping</a>
            </div>
        </div>
    </div>
</div>

<script src="<c:url value="/resources/lib/jquery-3.1.1.min.js" />"></script>
<script src="<c:url value="/resources/lib/bootstrap/js/bootstrap.min.js" />"></script>
</body>
</html>