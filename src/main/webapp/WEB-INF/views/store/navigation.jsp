<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<style>
    .alert {
        padding: 20px;
        background-color: #f44336;
        color: white;
        opacity: 1;
        transition: opacity 0.6s;
        margin-bottom: 15px;
    }

    .alert.success {
        background-color: #4CAF50;
    }

    .alert.info {
        background-color: #2196F3;
    }

    .alert.warning {
        background-color: #ff9800;
    }

    .closebtn {
        margin-left: 15px;
        color: white;
        font-weight: bold;
        float: right;
        font-size: 22px;
        line-height: 20px;
        cursor: pointer;
        transition: 0.3s;
    }

    .closebtn:hover {
        color: black;
    }
</style>
<div class="col-md-3 col-sm-3">
    <div class="column">
        <ul class="nav nav-pills nav-stacked shadow-depth-1">
            <li><a href="/flyleaf" style="color: white; background-color: gray" class="active2"><span
                    class="glyphicon glyphicon-th-large"></span>Back to Main Page</a></li>
            <li><a href="${pageContext.servletContext.contextPath}/mystores"
                   style="color: white; background-color: gray" class="active2"><span
                    class="glyphicon glyphicon-th-large"></span>My Stores</a></li>
            <li><a href="${pageContext.servletContext.contextPath}/mystores/add"
                   style="color: white; background-color: gray">
                <span class="glyphicon glyphicon-flag"></span>Add Store</a>
            </li>
            <li><a href="${pageContext.servletContext.contextPath}/mystores/activity/0"
                   style="color: white; background-color: gray"><span class="glyphicon glyphicon-flag"></span>My
                Activities</a></li>
            <li><a href="${pageContext.servletContext.contextPath}/mystores/order/0"
                   style="color: white; background-color: gray"><span class="glyphicon glyphicon-flag"></span>Customer
                Orders</a></li>
        </ul>
    </div>
</div>
