<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="springForm" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title><c:out value="Add New Book"/></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
<jsp:include page="navigation.jsp"/>
<div class="col-md-9 col-sm-9">
    <div class="panel panel-default shadow-depth-1">
        <div class="panel-heading"><c:out value="Add New Book"/></div>
        <div class="panel-body">
            <div class="row">
                <jsp:include page="logMessage.jsp"/>
                <springForm:form action="publisher" method="GET">
                    <div class="form-group">
                        <label class="control-label col-sm-2"><c:out value="Publisher"/></label>
                        <select class="form-control" name="selectedPublisher" required>
                            <option value= <c:out value=""/>><c:out value="Select the Publisher"/></option>
                            <c:forEach items="${publisherList}" var="publisher">
                            <option value= <c:out value="${publisher.id}"/>>
                                    <c:out value="${publisher.name} (publisher: ${publisher.publisherInfo})"/>
                                </option>
                                </c:forEach>
                            <option value= <c:out value="otherPublisher"/>><c:out value="Not in the List, Create new."/></option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2"><c:out value="How Many Authors?"/></label>
                        <div class="col-sm-10">
                            <input type="number" name="noOfAuthors" class="form-control"
                                   placeholder="Enter Number of Authors" required="" min="1">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" class="btn btn-default"><c:out value="Submit"/></button>
                        </div>
                    </div>
                </springForm:form>
            </div>
        </div>
    </div>
</div>


</body>
</html>