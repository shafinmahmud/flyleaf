<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>FlyLeaf</title>

    <link href="<c:url value="/resources/css/home-style.css" />" rel="stylesheet" type="text/css"/>

    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>


    <style type="text/css">
        .marketing {
            text-align: center;
            margin-bottom: 20px;
        }

        .divider {
            margin: 80px 0;
        }

        hr {
            border: solid 1px #eee;
        }

        .thumbnail img {
            width: 100%;
        }

        .navbar-default .navbar-brand {
            color: #000000;
        }

        hr {
            display: block;
            height: 10px;
            border: 0;
            border-top: 1px solid #ccc;
            margin-top: 30px;
            margin-bottom: 30px;
            margin-left: auto;
            margin-right: auto;
        }

    </style>
</head>
<body>

<hr>

<nav class="navbar-inverse">
    <div>
        <ul class="nav nav-justified">
            <li class="active"><a href="<c:url value="/categories"/>"><spring:message
                    code="label.category"/></a></li>
            <li><a href="<c:url value="/authors"/>"><spring:message
                    code="label.author"/></a></li>
            <li><a href="<c:url value="/publishers"/>"><spring:message
                    code="label.publisher"/></a></li>
            <li><a href="<c:url value="/stores"/>"><spring:message
                    code="label.store"/></a></li>
        </ul>
    </div>
</nav>
<!-- Main Navigation Bar End -->

<hr>

<!-- Slide Show -->
<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
        <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
        <li data-target="#carousel-example-generic" data-slide-to="1"></li>
        <li data-target="#carousel-example-generic" data-slide-to="2"></li>
        <li data-target="#carousel-example-generic" data-slide-to="3"></li>
    </ol>

    <div class="carousel-inner" role="listbox">
        <div class="item active">
            <img src="<c:url value ="/resources/images/banner.jpg" />" alt="img1">

            <div class="carousel-caption">
                <h3 style="color: #000000"><c:out value="${topRatedProductListForCarousal[0].book.title}"/></h3>

                <p style="color: #000000"><c:out value="${topRatedProductListForCarousal[0].book.publisher.name}"/></p>
            </div>
        </div>
        <div class="item">
            <img src="<c:url value ="/resources/images/banner.jpg" />" alt="img1">

            <div class="carousel-caption">
                <h3 style="color: #000000"><c:out value="${topRatedProductListForCarousal[1].book.title}"/></h3>

                <p style="color: #000000"><c:out value="${topRatedProductListForCarousal[1].book.publisher.name}"/></p>
            </div>
        </div>
        <div class="item">
            <img src="<c:url value ="/resources/images/banner.jpg" />" alt="img1">

            <div class="carousel-caption">
                <h3 style="color: #000000"><c:out value="${topRatedProductListForCarousal[2].book.title}"/></h3>

                <p style="color: #000000"><c:out value="${topRatedProductListForCarousal[2].book.publisher.name}"/></p>
            </div>
        </div>
        <div class="item">
            <img src="<c:url value ="/resources/images/banner.jpg" />" alt="img1">

            <div class="carousel-caption">
                <h3 style="color: #000000"><c:out value="${topRatedProductListForCarousal[3].book.title}"/></h3>

                <p style="color: #000000"><c:out value="${topRatedProductListForCarousal[3].book.publisher.name}"/></p>
            </div>
        </div>
    </div>
    <!-- Controls -->
    <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>
<div class="clearfix" style="margin-bottom:20px;"></div>
<div class="container">

    <hr>

    <div class="container">
        <nav class="navbar-inverse">
            <div>
                <ul class="nav nav-justified">
                    <li><a href="<c:url value="/popular/categories"/>"><spring:message
                            code="label.popular.categories"/></a></li>
                    <li><a href="<c:url value="/popular/authors"/>"><spring:message
                            code="label.popular.author"/></a></li>
                    <li><a href="<c:url value="/popular/publishers"/>"><spring:message
                            code="label.popular.publisher"/></a></li>
                    <li><a href="<c:url value="/popular/stores"/>"><spring:message
                            code="label.popular.store"/></a></li>
                </ul>

            </div>
        </nav>

        <div class="row marketing">

            <c:forEach var="popularObject" items="${popularObjectList}">
                <a href="<c:url value="/${type}/${popularObject.id}"/>">
                    <div class="col-sm-6 col-md-3">
                        <div class="card">
                            <img src="<c:url value ="/resources/images/categoryicon.jpg" />">

                            <div class="well">${popularObject.name}</div>

                        </div>
                    </div>
                </a>
            </c:forEach>

        </div>

        <hr class="divider">
        <div class="tab-content">
            <div id="mostRatedBooks" class="tab-pane fade in active">
                <h3><spring:message code="label.topRatedBooks"/></h3>
            </div>
        </div>

        <div class="row">

            <c:forEach var="product" items="${topRatedBookList}">
                <a href="<c:url value="/product/${product.id}"/>">
                    <div class="col-sm-6 col-md-3">
                        <div class="card">
                            <img src="<c:url value ="/resources/images/categoryicon.jpg" />">

                            <div class="well">${product.book.title}</div>
                        </div>
                    </div>
                </a>
            </c:forEach>

        </div>

        <!-- Pagination Starts -->
        <div class="text-center">
            <ul class="pagination">
                <%--For displaying Previous link except for the 1st page --%>
                <c:if test="${currentPage != 1}">
                    <li><a href="<c:url value="${viewUrl}?page=${currentPage - 1}"/>"><< previous</a></li>
                </c:if>
                <%--For displaying Page numbers.
                The when condition does not display a link for the current page--%>
                <c:forEach begin="1" end="${noOfPages}" var="i">
                    <c:choose>
                        <c:when test="${currentPage eq i}">
                            <li class="active"><a>${i}</a></li>
                        </c:when>
                        <c:otherwise>
                            <li><a href="<c:url value="${viewUrl}?page=${i}"/>">${i}</a></li>
                        </c:otherwise>
                    </c:choose>
                </c:forEach>

                <%--For displaying Next link --%>
                <c:if test="${currentPage lt noOfPages}">
                    <li><a href="<c:url value="${viewUrl}?page=${currentPage + 1}"/>">next >></a></li>
                </c:if>
            </ul>
        </div>

        <hr class="divider">

        <footer>
            <p class="pull-right"><a href="#">Back To Top</a></p>

            <p>Designed By Company . <a href="#">Privacy</a> . <a href="#">Terms</a></p>
        </footer>
    </div>
</div>

<script src="<c:url value="/resources/lib/jquery-3.1.1.min.js" />"></script>
<script src="<c:url value="/resources/lib/bootstrap/js/bootstrap.min.js" />"></script>
</body>
</html>