<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>

<body>
<div class="container">

    <div class="row">

        <jsp:include page="navigation.jsp"/>

        <div class="col-md-9 col-sm-9">
            <%--Show number of pending orders--%>
            <div class="row">
                <div class="col-md-4 column">
                    <div class="panel panel-primary shadow-depth-1">
                        <div class="panel-heading">
                            <div class="panel-title">
                                <span><spring:message code="label.pendingOrders"/></span>
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="row clearfix">
                                <div class="col-md-8 column">
                                    <p style="text-align: center; font-size: 20px;">
                                        ${countPendingOrders}
                                        <small><spring:message code="label.orders"/></small>
                                    </p>
                                </div>
                                <div class="col-md-4 column">
                                    <span class="pull-right fa fa-users fa-5x libreStatsIcon"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <%--Show number of processing orders--%>
                <div class="col-md-4 column">
                    <div class="panel panel-success shadow-depth-1">
                        <div class="panel-heading">
                            <div class="panel-title">
                                <span><spring:message code="label.processingOrders"/></span>
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="row clearfix">
                                <div class="col-md-8 column">
                                    <p style="text-align: center; font-size: 20px;">
                                        ${countProcessingOrders}
                                        <small><spring:message code="label.orders"/></small>
                                    </p>
                                </div>
                                <div class="col-md-4 column">
                                    <span class="pull-right fa fa-users fa-5x libreStatsIcon"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4 column">
                    <a class="btn btn-info" href="${pageContext.servletContext.contextPath}/admin/orders">
                        <spring:message code="button.ongoingOrders"/>
                    </a>
                </div>
            </div>
            <br>

            <%--Show number of New Product Requests--%>
            <div class="row">
                <div class="col-md-4 column">
                    <div class="panel panel-warning shadow-depth-1">
                        <div class="panel-heading">
                            <div class="panel-title">
                                <span><spring:message code="label.newProduct"/></span>
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="row clearfix">
                                <div class="col-md-8 column">
                                    <p style="text-align: center; font-size: 20px;">
                                        ${countNotVerifiedProducts}
                                        <small><spring:message code="label.request"/></small>
                                    </p>
                                </div>
                                <div class="col-md-4 column">
                                    <span class="pull-right fa fa-users fa-5x libreStatsIcon"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4 column">
                    <a class="btn btn-info" href="${pageContext.servletContext.contextPath}/admin/newProducts">
                        <spring:message code="button.productRequests"/>
                    </a>
                </div>
            </div>
            <br>

            <%--Show number of Pending Stores--%>
            <div class="row">
                <div class="col-md-4 column">
                    <div class="panel panel-danger shadow-depth-1">
                        <div class="panel-heading">
                            <div class="panel-title">
                                <span><spring:message code="label.pendingStore"/></span>
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="row clearfix">
                                <div class="col-md-8 column">
                                    <p style="text-align: center; font-size: 20px;">
                                        ${countPendingStores}
                                        <small><spring:message code="label.stores"/></small>
                                    </p>
                                </div>
                                <div class="col-md-4 column">
                                    <span class="pull-right fa fa-users fa-5x libreStatsIcon"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4 column">
                    <a class="btn btn-info" href="${pageContext.servletContext.contextPath}/admin/stores">
                        <spring:message code="button.viewStores"/>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</body>