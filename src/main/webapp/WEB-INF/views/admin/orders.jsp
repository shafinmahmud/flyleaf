<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>

<body>
<div class="container">

    <div class="row">

        <jsp:include page="navigation.jsp"/>

        <%--Show Pending Orders--%>
        <div class="col-md-9 col-sm-9">
            <div class="panel panel-default shadow-depth-1">
                <div class="panel-heading"><spring:message code="label.pendingOrders"/></div>

                <div class="panel-body">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th><spring:message code="label.orderStatus"/></th>
                            <th><spring:message code="label.customer"/></th>
                            <th><spring:message code="label.product(Quantity)"/></th>
                            <th><spring:message code="label.totalPrice"/></th>
                            <th><spring:message code="label.checkoutTime"/></th>
                            <th><spring:message code="button.viewDetails"/></th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach items="${pendingOrders}" var="pendingOrder">
                            <tr>
                                <td class="alert-info">${pendingOrder.orderStatus}</td>
                                <td>${pendingOrder.user.firstName}</td>
                                <td>
                                    <c:forEach items="${pendingOrder.orderedProductList}" var="orderedProduct">
                                        ${orderedProduct.product.book.title} (${orderedProduct.quantity})
                                        <br>
                                    </c:forEach>
                                </td>
                                <td>${pendingOrder.totalPrice}</td>
                                <td><fmt:formatDate value="${pendingOrder.checkoutTime}" type="both" dateStyle="long"
                                                    timeStyle="long"/></td>
                                <td>
                                    <a class="btn btn-info"
                                       href="${pageContext.servletContext.contextPath}/admin/order/${pendingOrder.id}">
                                        <spring:message code="button.viewDetails"/>
                                    </a>
                                </td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>

            <%--Show Processing Orders--%>
            <div class="panel panel-default shadow-depth-1">
                <div class="panel-heading"><spring:message code="label.processingOrders"/></div>

                <div class="panel-body">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th><spring:message code="label.orderStatus"/></th>
                            <th><spring:message code="label.customer"/></th>
                            <th><spring:message code="label.product(Quantity)"/></th>
                            <th><spring:message code="label.totalPrice"/></th>
                            <th><spring:message code="label.checkoutTime"/></th>
                            <th><spring:message code="button.viewDetails"/></th>
                        </tr>
                        </thead>
                        <tbody>

                        <c:forEach items="${processingOrders}" var="processingOrder">
                            <tr>
                                <td class="alert-success">${processingOrder.orderStatus}</td>
                                <td>${processingOrder.user.firstName}</td>
                                <td>
                                    <c:forEach items="${processingOrder.orderedProductList}" var="orderedProduct">
                                        ${orderedProduct.product.book.title} (${orderedProduct.quantity})
                                        <br>
                                    </c:forEach>
                                </td>
                                <td>${processingOrder.totalPrice}</td>
                                <td>
                                    <fmt:formatDate value="${processingOrder.checkoutTime}" type="both" dateStyle="long"
                                                    timeStyle="long"/>
                                </td>
                                <td>
                                    <a class="btn btn-info"
                                       href="${pageContext.servletContext.contextPath}/admin/order/${processingOrder.id}">
                                        <spring:message code="button.viewDetails"/>
                                    </a>
                                </td>
                            </tr>
                        </c:forEach>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

</div>
</body>