<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<html>
<head>
    <title>FlyLeaf :: Profile</title>
</head>
<body>
<div class="container">

    <br>
    <br>
    <br>

    <div class="row">

        <%@include file="../jspf/userNavPill.jspf" %>

        <div class="col-md-9 col-sm-9">

            <div class="panel panel-default shadow-depth-1">
                <div class="panel-heading">
                    <h4><spring:message code="panel.profile"/>&nbsp;</h4>
                </div>
                <div class="panel-body">
                    <form class="form-horizontal">
                        <div class="form-group">

                            <label class="control-label col-sm-2"><spring:message code="label.name"/> :</label>

                            <div class="col-sm-10">
                                <p class="form-control-static"><c:out
                                        value="${userProfileForm.firstName} ${userProfileForm.lastName}"/></p>
                            </div>

                            <label class="control-label col-sm-2"><spring:message code="label.email"/> :</label>

                            <div class="col-sm-10">
                                <p class="form-control-static"><c:out value="${userProfileForm.email}"/></p>
                            </div>

                            <label class="control-label col-sm-2"><spring:message code="label.phone"/> :</label>

                            <div class="col-sm-10">
                                <p class="form-control-static"><c:out value="${userProfileForm.phone}"/></p>
                            </div>

                            <label class="control-label col-sm-2"><spring:message code="label.address"/> :</label>

                            <div class="col-sm-10">
                                <p class="form-control-static"><c:out value="${userProfileForm.address}"/></p>
                            </div>

                            <label class="control-label col-sm-2"><spring:message code="label.dob"/> :</label>

                            <div class="col-sm-10">
                                <p class="form-control-static"><c:out value="${userProfileForm.dateOfBirth}"/></p>
                            </div>

                            <label class="control-label col-sm-2"><spring:message code="label.gender"/> :</label>

                            <div class="col-sm-10">
                                <p class="form-control-static"><c:out value="${userProfileForm.gender}"/></p>
                            </div>

                            <label class="control-label col-sm-2"><spring:message code="label.bio"/> :</label>

                            <div class="col-sm-10">
                                <p class="form-control-static"><c:out value="${userProfileForm.bio}"/></p>
                            </div>
                            <br>

                            <label class="control-label col-sm-2"></label>

                            <div class="col-sm-10">
                                <a class="btn btn-success"
                                   href="<c:url value="/user/profile-edit"/>"><spring:message
                                        code="button.editProfile"/></a>
                            </div>

                        </div>
                    </form>
                    <!-- Alert -->
                    <c:if test="${profileEditSuccess != null}">
                        <div class=" alert alert-success alert-dismissable fade in shadow-depth-2">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <strong>Success !</strong> Profile has been updated.
                        </div>
                    </c:if>

                </div>
            </div>

            <div class="panel panel-danger shadow-depth-1">
                <div class="panel-heading">
                    <a>
                        <spring:message code="panel.changePassword"/>
                    </a>
                </div>

                <div class="panel-body">

                    <form class="form-horizontal">
                        <fieldset>

                            <!-- Password input-->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="oldPassword">
                                    <spring:message code="label.oldPassword"/>
                                </label>

                                <div class="col-md-4">
                                    <input id="oldPassword" name="password" type="password" placeholder=""
                                           class="form-control input-md" required="">

                                </div>
                            </div>

                            <!-- Password input-->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="newPassword">
                                    <spring:message code="label.newPassword"/>
                                </label>

                                <div class="col-md-4">
                                    <input id="newPassword" name="newPassword" type="password" placeholder=""
                                           class="form-control input-md" required="">

                                </div>
                            </div>

                            <!-- Password input-->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="NewPasswordRetyped">
                                    <spring:message code="label.verifyNewPassword"/>
                                </label>

                                <div class="col-md-4">
                                    <input id="newpasswordretyped" name="newPasswordRetyped" type="password"
                                           class="form-control input-md" required="">

                                </div>
                            </div>

                            <!-- Button (Double) -->
                            <div class="form-group">
                                <label class="col-md-4 control-label"></label>

                                <div class="col-md-8">
                                    <button class="btn btn-danger">Submit</button>
                                </div>
                            </div>

                        </fieldset>
                    </form>

                </div>
            </div>

        </div>

    </div>
</div>

</body>
</html>
