<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<html>
<head>
    <title>FlyLeaf :: Overview</title>
</head>
<body>
<div class="container">

    <br>
    <br>
    <br>

    <div class="row">

        <%@include file="../jspf/userNavPill.jspf" %>

        <div class="col-md-9 col-sm-9">

            <div class="col-md-4 column">
                <div class="panel panel-warning shadow-depth-1">
                    <div class="panel-heading">
                        <div class="panel-title">
                            <span>Cart</span>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="row clearfix">
                            <div class="col-md-8 column">
                                <p style="text-align: center; font-size: 20px;">
                                    <c:out value="${cartCount}"/>
                                </p>
                            </div>
                            <div class="col-md-4 column">
                                <span class="pull-right fa fa-users fa-5x libreStatsIcon"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-4 column">
                <div class="panel panel-info shadow-depth-1">
                    <div class="panel-heading">
                        <div class="panel-title">
                            <span>Reviewed</span>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="row clearfix">
                            <div class="col-md-8 column">
                                <p style="text-align: center; font-size: 20px;">
                                    <c:out value="${reviewCount}"/>
                                </p>
                            </div>
                            <div class="col-md-4 column">
                                <span class="pull-right fa fa-users fa-5x libreStatsIcon"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-4 column">
                <div class="panel panel-success shadow-depth-1">
                    <div class="panel-heading">
                        <div class="panel-title">
                            <span>Purchased</span>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="row clearfix">
                            <div class="col-md-8 column">
                                <p style="text-align: center; font-size: 20px;">
                                    <c:out value="${purchaseCount}"/>
                                </p>
                            </div>
                            <div class="col-md-4 column">
                                <span class="pull-right fa fa-users fa-5x libreStatsIcon"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-9 col-sm-9">
            <div class="panel panel-default shadow-depth-1">
                <div class="panel-heading">Recent Order&nbsp;
                    <span class="status status-success">Completed</span>
                </div>
                <div class="panel-body">

                    <div class="row">

                        <c:forEach items="${recentOrder.productList}" var="product">

                            <!-- BEGIN PRODUCTS -->
                            <div class="col-md-3 col-sm-3">
    		                <span class="thumbnail">
                                <div class="row">
                                    <img class="col-md-8"
                                         src="https://s3-ap-southeast-1.amazonaws.com/rokomari110/product/imgrok0610_8022.GIF"
                                         alt="book">
                                </div>

                                <div class="row product-desc">
                                    <div class="col-md-10">
                                        <h4 style="text-align: center"><c:out value="${product.bookTitle}"/></h4>

                                        <p style="text-align: center"><c:out value="${product.author}"/></p>

                                        <div class="ratings">
                                            <span class="glyphicon glyphicon-star"></span>
                                            <span class="glyphicon glyphicon-star"></span>
                                            <span class="glyphicon glyphicon-star"></span>
                                            <span class="glyphicon glyphicon-star"></span>
                                            <span class="glyphicon glyphicon-star-empty"></span>
                                        </div>
                                        <hr class="line">
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12">
                                                <p class="price">TK <c:out value="${product.price}"/></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
    		                 </span>
                            </div>
                        </c:forEach>


                    </div>

                </div>
            </div>

            <div class="panel panel-default shadow-depth-1">
                <div class="panel-heading">My Recent Activity</div>
                <div class="panel-body">
                    <div class="row">


                    </div>


                </div>
            </div>

        </div>
    </div>
</div>

</div>
</body>
</body>
</html>
