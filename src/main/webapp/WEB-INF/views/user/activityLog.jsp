<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<html>
<head>
    <title>FlyLeaf :: Activity Log</title>

</head>
<body>
<div class="container">

    <br>
    <br>
    <br>

    <div class="row">

        <%@include file="../jspf/userNavPill.jspf" %>

        <div class="col-md-9 col-sm-9">

            <div class="panel panel-default shadow-depth-1">
                <div class="panel-heading">Activity Log&nbsp;
                </div>
                <div class="panel-body">

                    <div class="row">

                        <c:forEach items="${logList}" var="log">
                            <div class="col-sm-12 col-md-12">
                                <div class="alert-message alert-message-success">
                                    <h4><c:out value="${log.createdAt}"/> | <c:out value="${log.activityType}"/></h4>
                                    <p> <c:out value="${log.message}"/></p>
                                </div>

                            </div>
                        </c:forEach>

                    </div>

                </div>

            </div>

        </div>
    </div>
</div>

</body>
</html>
